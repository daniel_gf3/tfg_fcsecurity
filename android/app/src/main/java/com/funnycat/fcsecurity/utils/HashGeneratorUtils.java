package com.funnycat.fcsecurity.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashGeneratorUtils {

    private static String TAG= "HashGeneratorUtils";

    private static int SHA1_SIZE=40, SHA256_SIZE=64, MD5_SIZE=32;


    private HashGeneratorUtils() {

    }

    // STRING

    public static String generateMD5(String message){
        String result = hashString(message, "MD5");
        return (result == null || result.length()!=MD5_SIZE)?null:result;
    }

    public static String generateSHA1(String message){
        String result = hashString(message, "SHA-1");
        return (result == null || result.length()!=SHA1_SIZE)?null:result;
    }

    public static String generateSHA256(String message){
        String result = hashString(message, "SHA-256");
        return (result == null || result.length()!=SHA256_SIZE)?null:result;
    }

    // FILE

    public static String generateMD5F(File file){
        String result =hashFile(file, "MD5");
        return (result == null || result.length()!=MD5_SIZE)?null:result;
    }

    public static String generateSHA1F(File file){
        String result = hashFile(file, "SHA-1");
        return (result == null || result.length()!=SHA1_SIZE)?null:result;
    }

    public static String generateSHA256F(File file){
        String result = hashFile(file, "SHA-256");
        return (result == null || result.length()!=SHA256_SIZE)?null:result;
    }


    public static String generateMD5F(String path){
        return generateMD5F(new File(path));
    }

    public static String generateSHA1F(String path){
        return generateSHA1F(new File(path));
    }

    public static String generateSHA256F(String path){
        try {
            return generateSHA256F(new File(path));
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }


    private static String hashString(String message, String algorithm){

        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));

            return convertByteArrayToHexString(hashedBytes);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex){
            ex.printStackTrace();
        }
        return null;
    }


    private static String hashFile(File file, String algorithm){
        try{
            FileInputStream inputStream = new FileInputStream(file);
            MessageDigest digest = MessageDigest.getInstance(algorithm);

            byte[] bytesBuffer = new byte[1024];
            int bytesRead = -1;

            while ((bytesRead = inputStream.read(bytesBuffer)) != -1) {
                digest.update(bytesBuffer, 0, bytesRead);
            }

            byte[] hashedBytes = digest.digest();

            return convertByteArrayToHexString(hashedBytes);
        } catch (NoSuchAlgorithmException | IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuilder stringB = new StringBuilder();
        for (byte arrayByte : arrayBytes) {
            stringB.append(Integer.toString((arrayByte & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringB.toString();
    }
}