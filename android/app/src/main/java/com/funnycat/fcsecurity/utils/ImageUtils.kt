package com.funnycat.fcsecurity.utils

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.newFixedThreadPoolContext
import java.io.File

object ImageUtils {

    // CoroutineContext running on background threads.
    internal val Background = newFixedThreadPoolContext(Runtime.getRuntime().availableProcessors() * 2, "Loader")

    fun getIcon(iconPath: String?) = iconPath?.let { BitmapFactory.decodeFile(File(it).absolutePath, BitmapFactory.Options()) }

//    fun getIcon(icon: Icon) = getIcon(icon.data)

    fun getIcon(iconBA: ByteArray?) = iconBA?.let { BitmapFactory.decodeByteArray(iconBA, 0, iconBA.size) }

    inline fun loadAsyncIcon(iView: ImageView, fakeView: View, crossinline loadFunction: () -> Drawable) {
        val view = iView//.asReference()
        val fake = fakeView//.asReference()

        launch (context = UI) {
            showFakeImage(view, fake)
            val loadImage = async {
                try {
                    loadFunction()
                } catch (e: Exception) {
                    null
                }
            }
            loadImage.await()?.let {
                iView.setImageDrawable(loadImage.await())
                showImage(view, fake)
            }
        }
    }

    fun showImage(iView: ImageView, fakeView: View) {
        fakeView.visibility = View.INVISIBLE
        iView.visibility = View.VISIBLE
//        fakeView.visibility = View.INVISIBLE
    }

    fun showFakeImage(iView: ImageView, fakeView: View) {
        fakeView.visibility = View.VISIBLE
        iView.visibility = View.INVISIBLE
//        fakeView.visibility = View.VISIBLE
    }

    fun dpToPx(context: Context, dp: Float): Int {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.resources.displayMetrics))
    }
}