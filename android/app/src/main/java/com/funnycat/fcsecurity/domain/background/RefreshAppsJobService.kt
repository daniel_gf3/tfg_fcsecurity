package com.funnycat.fcsecurity.domain.background

import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import com.funnycat.fcsecurity.data.datasources.database.AppDao
import javax.inject.Inject
import com.firebase.jobdispatcher.*
import com.funnycat.fcsecurity.data.datasources.system.AppSystemSource
import com.funnycat.fcsecurity.data.repositories.AppRepository.Companion.SP_ANALYSIS_COMPLETED
import com.funnycat.fcsecurity.utils.TimeUtils
import dagger.android.AndroidInjection


class RefreshAppsJobService : JobService() {

    @Inject
    lateinit var appDao: AppDao

    @Inject
    lateinit var firebaseJobDispatcher: FirebaseJobDispatcher

    @Inject
    lateinit var sp: SharedPreferences

    @Inject
    lateinit var systemSource: AppSystemSource


    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }


    override fun onStopJob(job: JobParameters) = true // Answers the question: "Should this job be retried?"

    override fun onStartJob(job: JobParameters): Boolean {
        task(job).start()
        return true // Answers the question: "Is there still work going on?"
    }


    private fun task(job: JobParameters) = Thread {
        Log.d(TAG, "start task")

        if (appDao.numApps() >= 0) jobFinished(job, true)

        // running in a background thread
        val listApps = systemSource.getInstalledApps()
        if (appDao.numApps() <= 0) {
            appDao.insert(*listApps.toTypedArray())
            sp.edit { putBoolean(SP_ANALYSIS_COMPLETED, true) }
        } else { // Update
            Log.d(TAG, "Update DB")
            val appsInDB = appDao.getAllApps()
            val currentHashes = listApps.map { it.hash }
            val dbHashes = appsInDB.map { it.hash }

            val deletedApps = appsInDB.filter { !currentHashes.contains(it.hash) } // Not in current apps
            val addedApps = listApps.filter { !dbHashes.contains(it.hash) }

            appDao.delete(*deletedApps.toTypedArray())
            appDao.insert(*addedApps.toTypedArray())
        }
        jobFinished(job, false)
    }


    companion object {
        private const val TAG = "RefreshAppsJobService"

        private fun makeJob(dispatcher: FirebaseJobDispatcher) : Job {

            val windowsStart = TimeUtils.minutesToSeconds(0)
            val windowsEnd = windowsStart +  TimeUtils.minutesToSeconds(0)

            Log.d(TAG, "next work start in: [$windowsStart - $windowsEnd]")

            return dispatcher.newJobBuilder()
                    // the JobService that will be called
                    .setService(RefreshAppsJobService::class.java)
                    // uniquely identifies the job
                    .setTag(RefreshAppsJobService::class.java.simpleName)
                    // one-off job
                    .setRecurring(false)
                    // persist forever
                    .setLifetime(Lifetime.FOREVER)
                    // start between windowsStart and windowsEnd seconds from now
                    .setTrigger(Trigger.executionWindow(windowsStart, windowsEnd))
                    // don't overwrite an existing job with the same tag
                    .setReplaceCurrent(false)
                    // retry with exponential backoff
                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                    // constraints that need to be satisfied for the job to run
                    .setConstraints(Constraint.ON_UNMETERED_NETWORK)
                    .build()
        }

        fun launchJob(dispatcher: FirebaseJobDispatcher) {
            Log.d(TAG, "programJobs")
            val job = makeJob(dispatcher)
            dispatcher.mustSchedule(job)
        }
    }

}