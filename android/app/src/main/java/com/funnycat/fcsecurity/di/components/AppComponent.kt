package com.funnycat.fcsecurity.di.components

import com.funnycat.fcsecurity.di.MyApp
import com.funnycat.fcsecurity.di.modules.*
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton




@Singleton
@Component(modules = [AndroidInjectionModule::class,
    BuildActivityModule::class,
    BuildServiceModule::class,
    AppModule::class,
    NetModule::class])
interface AppComponent {
    fun inject(app: MyApp)
}