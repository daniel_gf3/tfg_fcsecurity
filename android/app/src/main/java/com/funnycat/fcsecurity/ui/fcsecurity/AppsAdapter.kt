package com.funnycat.fcsecurity.ui.fcsecurity

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.ui.commons.OnItemListener
import android.widget.ImageView
import androidx.core.util.Pair
import androidx.recyclerview.widget.DiffUtil
import com.funnycat.fcsecurity.R
import com.funnycat.fcsecurity.utils.PackageUtils
import com.funnycat.fcsecurity.utils.ImageUtils
import kotlinx.android.synthetic.main.row_app.view.*
import java.lang.Exception


class AppsAdapter (private var data: MutableList<App>? = null, var listener: OnItemListener? = null) : RecyclerView.Adapter<AppsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_app, parent, false))
    }

    override fun getItemCount() = data?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        data?.let { holder.bin(it[position]) }
    }

    fun updateData(newData: List<App>) {
        if (data != null) {
            val postDiffCallback = AppDiffCallback(data!!, newData)
            val diffResult = DiffUtil.calculateDiff(postDiffCallback)
            data!!.clear()
            data!!.addAll(newData)
            diffResult.dispatchUpdatesTo(this)
        } else {
            data = newData.toMutableList()
            notifyDataSetChanged()
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bin(app: App) {
            with(itemView) {
                tv_name.text = app.name
                tv_package.text = app.package_name
                iv_own.setImageDrawable(context.getDrawable(if (app.user_app) R.drawable.ic_user else R.drawable.ic_system))
                try {
                    loadImage(iv_logo, iv_fake_icon, app)
                } catch (e: Exception) {
                    Log.e(TAG, "Error with ${app.package_name}")
                }

                listener?.let { it ->
                    val sharedLogo = Pair<View, String>(iv_logo, "shared_logo")
                    setOnClickListener { listener?.onClickListener (app.hash, sharedLogo) }
                }
            }
        }

        private fun loadImage(iView: ImageView, fakeView: View, app: App) {
            ImageUtils.loadAsyncIcon(iView, fakeView) {
                if (app.icon != null) app.icon!!
                else PackageUtils.getLogo(iView.context, app.package_name)!!.apply { app.icon = this }
            }
        }
    }

    internal inner class AppDiffCallback(private val oldPosts: List<App>, private val newPosts: List<App>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldPosts.size
        }

        override fun getNewListSize(): Int {
            return newPosts.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldPosts[oldItemPosition].id == newPosts[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldPosts[oldItemPosition] == newPosts[newItemPosition]
        }
    }

    companion object {
        private const val TAG = "AppsAdapter"
    }
}