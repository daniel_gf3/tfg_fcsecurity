package com.funnycat.fcsecurity.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.funnycat.fcsecurity.di.key.ViewModelKey
import com.funnycat.fcsecurity.ui.fcsecurity.FCSecurityViewModel
import com.funnycat.fcsecurity.di.ViewModelFactory
import com.funnycat.fcsecurity.ui.detailsapp.DetailsAppViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

//@Module
//abstract class ViewModelModule {

//    @Binds
//    @IntoMap
//    @ViewModelKey(FCSecurityViewModel::class)
//    abstract fun bindFCSecurityViewModel(ViewModel: FCSecurityViewModel): ViewModel
//
////    @Binds
////    @IntoMap
////    @ViewModelKey(AppViewModel::class)
////    abstract fun bindAppViewModel(ViewModel: AppViewModel): ViewModel
//
//    @Binds
//    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory





@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FCSecurityViewModel::class)
    abstract fun bindFCSecurityViewModel(fcSecurityViewModel: FCSecurityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsAppViewModel::class)
    abstract fun bindDetailsAppViewModel(detailsViewModel: DetailsAppViewModel): ViewModel


    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}



