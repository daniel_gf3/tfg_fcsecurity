package com.funnycat.fcsecurity.domain.usercases

import androidx.lifecycle.LiveData
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.data.repositories.AppRepository
import javax.inject.Inject

class LoadAllAppsUseCase @Inject constructor(private val repository: AppRepository) : Command<Unit, LiveData<List<App>>> {

    override fun execute(parameters: Unit): LiveData<List<App>> {
        return repository.getAllApps()
    }
}