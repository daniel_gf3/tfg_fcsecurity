package com.funnycat.fcsecurity.utils

import java.io.File


object UnitUtils {
    fun getMegaBytes(file: File): Long {
        // Get length of file in bytes
        val fileSizeInBytes = file.length()
        // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
        val fileSizeInKB = fileSizeInBytes / 1024
        // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        return fileSizeInKB / 1024
    }

    fun getMegaBytes(path: String) = getMegaBytes(File(path))

    fun getStringSize(file: File): String {
        return try {
            val sizeKB = file.length() / 1024.toDouble()
            val sizeMB = sizeKB / 1024
            if (sizeMB > 1.0) "${roundDecimals(sizeMB, 2)} MB" else "${sizeKB.toInt()}  KB"
        } catch (e: Exception) {
            ""
        }
    }

    fun getStringSize(path: String) = getStringSize(File(path))

    private fun roundDecimals(value: Double, numOfDecimals: Int): Double {
        var result: Double = value
        val intPart: Int = Math.floor(result).toInt()
        result = (result - intPart) * Math.pow(10.0, numOfDecimals.toDouble())
        result = Math.round(result).toDouble()
        result = result / Math.pow(10.0, numOfDecimals.toDouble()) + intPart
        return result
    }
}