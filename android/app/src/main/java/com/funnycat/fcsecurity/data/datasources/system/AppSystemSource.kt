package com.funnycat.fcsecurity.data.datasources.system

import android.content.Context
import com.funnycat.fcsecurity.data.models.AppDataMapper
import com.funnycat.fcsecurity.utils.PackageUtils
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppSystemSource @Inject constructor(private val context: Context) {

    fun getInstalledApps() = AppDataMapper().convertToDomain(context, PackageUtils.calculateAllApkHashes(context.packageManager))
}