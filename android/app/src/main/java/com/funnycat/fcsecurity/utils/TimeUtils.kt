package com.funnycat.fcsecurity.utils

/**
 * Created by daniel on 02/04/2017.
 */

object TimeUtils {
    fun minutesToSeconds(minutes: Int): Int {
        return minutes * 60
    }

    fun hoursToSeconds(hours: Int): Int {
        return minutesToSeconds(hours * 60)
    }

    fun daysToSeconds(days: Int): Int {
        return hoursToSeconds(days * 24)
    }

    fun miliSecondsToSeconds(miliSeconds: Long): Long {
        return miliSeconds / 1000
    }

}
