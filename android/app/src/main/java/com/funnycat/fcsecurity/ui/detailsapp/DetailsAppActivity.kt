package com.funnycat.fcsecurity.ui.detailsapp

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.funnycat.fcsecurity.R
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.data.models.Status
import com.funnycat.fcsecurity.utils.ImageUtils
import com.funnycat.fcsecurity.utils.PackageUtils
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_details_app.*
import kotlinx.android.synthetic.main.content_details_app.*
import javax.inject.Inject
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.funnycat.fcsecurity.data.models.TypeApp
import com.funnycat.fcsecurity.utils.copyToClipboard
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.android.HasActivityInjector


class DetailsAppActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: DetailsAppViewModel

    @Inject lateinit var mFragmentInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var hash: String
    private var isFirstLoad = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_app)
        toolbar_layout.setExpandedTitleColor(resources.getColor(android.R.color.transparent))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        hash = intent.extras.getString(ARG_HASH_KEY, "")
        configureDagger()
        setupViewModel(hash)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home   -> {
                supportFinishAfterTransition()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun supportFragmentInjector() = mFragmentInjector

    // -----------------
    // CONFIGURATION
    // -----------------

    private fun configureDagger() {
        AndroidInjection.inject(this)
    }


    private fun setupViewModel(hash: String) {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailsAppViewModel::class.java)
        viewModel.loadApp(hash).observe(this, Observer { app ->
            app?.let { mApp ->
                Log.d(TAG, "Receiving database update from LiveData")
                tv_name.text = mApp.name
                tv_package_name.text = mApp.package_name
                loadImage(iv_logo, iv_logo, mApp)
                tv_size.text = mApp.size
                tv_sha256.text = mApp.hash
                tv_type.text = (if (mApp.user_app) TypeApp.USER else TypeApp.SYSTEM).name
                tv_result.text = if (mApp.status == Status.ANALIZED) {
                    if (mApp.positive) "MALWARE" else "CLEAN"
                } else mApp.status.name

                changeFabVisibility(fab, mApp.status == Status.UNKNOWN)

                tv_sha256.setOnClickListener {
                    baseContext.copyToClipboard(mApp.hash)
                    showSnackbar(R.string.hash_copied_to_clipboard)
                }
                fab.setOnClickListener {
                    viewModel.uploadApp(mApp.hash)
                    showSnackbar(R.string.uploading)
                }

                if (!isFirstLoad && (mApp.status == Status.ANALIZED)) Snackbar.make(container, "Changed detected", Snackbar.LENGTH_SHORT).show()
                isFirstLoad = false
            }
        })
    }

    private fun loadImage(iView: ImageView, fakeView: View, app: App) {
        ImageUtils.loadAsyncIcon(iView, fakeView) {
            if (app.icon != null) app.icon!!
            else PackageUtils.getLogo(iView.context, app.package_name)!!.apply { app.icon = this }
        }
    }

    private fun showSnackbar(textID: Int) {
        Snackbar.make(container, textID, Snackbar.LENGTH_LONG).show()
    }

    @SuppressLint("RestrictedApi")
    private fun changeFabVisibility(fab1: FloatingActionButton, visible: Boolean) {
        val p = fab1.layoutParams as CoordinatorLayout.LayoutParams
        p.anchorId = View.NO_ID
        fab1.layoutParams = p
        fab1.visibility = if (visible) View.VISIBLE else View.GONE
    }

    companion object {
        private const val TAG = "DetailsAppActivity"

        private const val ARG_HASH_KEY = "hash"

        fun getCallingIntent(context: Context, hash: String): Intent {
            val callingIntent = Intent(context, DetailsAppActivity::class.java)
            callingIntent.putExtra(ARG_HASH_KEY, hash)
            return callingIntent
        }
    }

}
