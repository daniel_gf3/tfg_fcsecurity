package com.funnycat.fcsecurity.domain.background

import android.util.Log
import com.funnycat.fcsecurity.data.datasources.database.AppDao
import javax.inject.Inject
import com.firebase.jobdispatcher.*
import com.funnycat.fcsecurity.utils.TimeUtils
import dagger.android.AndroidInjection


class UploadAllAppsJobService : JobService() {

    @Inject
    lateinit var appDao: AppDao

    @Inject
    lateinit var firebaseJobDispatcher: FirebaseJobDispatcher


    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }


    override fun onStopJob(job: JobParameters) = true // Answers the question: "Should this job be retried?"

    override fun onStartJob(job: JobParameters): Boolean {
        task(job).start()
        return true // Answers the question: "Is there still work going on?"
    }


    private fun task(job: JobParameters) = Thread {
        Log.d(TAG, "start task")

        if (appDao.numApps() >= 0) jobFinished(job, true)

        val firstUnknownApp = appDao.getFirstUnknownApp()

        if (firstUnknownApp == null) {
            Log.d(TAG, "stop task")
            jobFinished(job, false)

        } else {
            UploadAppJobService.launchJob(firebaseJobDispatcher, firstUnknownApp.hash)
            jobFinished(job, true)
        }
    }


    companion object {
        private const val TAG = "UploadAllAppsJobService"

        private fun makeJob(dispatcher: FirebaseJobDispatcher) : Job {

            val windowsStart = TimeUtils.minutesToSeconds(5)
            val windowsEnd = windowsStart +  TimeUtils.minutesToSeconds(30)

            Log.d(TAG, "next work start in: [$windowsStart - $windowsEnd]")

            return dispatcher.newJobBuilder()
                    // the JobService that will be called
                    .setService(UploadAllAppsJobService::class.java)
                    // uniquely identifies the job
                    .setTag(UploadAllAppsJobService::class.java.simpleName)
                    // one-off job
                    .setRecurring(false)
                    // persist forever
                    .setLifetime(Lifetime.FOREVER)
                    // start between windowsStart and windowsEnd seconds from now
                    .setTrigger(Trigger.executionWindow(windowsStart, windowsEnd))
                    // don't overwrite an existing job with the same tag
                    .setReplaceCurrent(false)
                    // retry with exponential backoff
                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                    // constraints that need to be satisfied for the job to run
                    .setConstraints(Constraint.ON_UNMETERED_NETWORK)
                    .build()
        }

        fun launchJob(dispatcher: FirebaseJobDispatcher) {
            Log.d(TAG, "programJobs")
            val job = makeJob(dispatcher)
            dispatcher.mustSchedule(job)
        }
    }

}