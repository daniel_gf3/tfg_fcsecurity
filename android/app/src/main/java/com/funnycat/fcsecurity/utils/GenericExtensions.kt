package com.funnycat.fcsecurity.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.Toast

fun Context.toast(messageResId: Int, duration: Int = Toast.LENGTH_LONG) =
        Toast.makeText(this, messageResId, duration).show()

fun Context.toast(message: String, duration: Int = Toast.LENGTH_LONG) =
        Toast.makeText(this, message, duration).show()


fun <T> Context.copyToClipboard(toCopy: T){
    val clip = ClipData.newPlainText("copy", toCopy.toString())
    (getSystemService(android.content.Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = clip
}