package com.funnycat.fcsecurity.di.modules

import com.funnycat.fcsecurity.ui.fcsecurity.FcsecurityFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeFCSecurityFragment() : FcsecurityFragment
}