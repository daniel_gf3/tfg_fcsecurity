package com.funnycat.fcsecurity.di

import android.app.Application
import android.content.Context
import com.funnycat.fcsecurity.di.components.DaggerAppComponent
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import dagger.android.HasActivityInjector
import android.app.Activity
import android.app.Service
import android.content.SharedPreferences
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.funnycat.fcsecurity.BuildConfig
import com.funnycat.fcsecurity.di.modules.AppModule
import com.funnycat.fcsecurity.di.modules.NetModule
import com.funnycat.fcsecurity.domain.background.MyFirebaseMessagingService
import com.funnycat.fcsecurity.domain.background.UploadAllAppsJobService
import com.google.firebase.iid.FirebaseInstanceId
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasServiceInjector
import javax.inject.Inject



class MyApp : Application(), HasActivityInjector, HasServiceInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>

    @Inject
    lateinit var sp: SharedPreferences

    @Inject
    lateinit var firebaseJobDispatcher: FirebaseJobDispatcher

    lateinit var refWatcher: RefWatcher

    override fun onCreate() {
        super.onCreate()
        instance = this

        DaggerAppComponent
                .builder()
                .appModule(AppModule(this@MyApp))
                .netModule(NetModule(BuildConfig.BASE_URL))
                .build()
                .inject(this@MyApp)

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        refWatcher = LeakCanary.install(this)

        loadFCMToken()
        programUploadAllApps()
    }

    override fun activityInjector() = dispatchingAndroidInjector

    override fun serviceInjector() = dispatchingServiceInjector


    private fun loadFCMToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                MyFirebaseMessagingService.saveFCMToken(sp, task.result.token)
            }
        }
    }

    private fun programUploadAllApps() {
        UploadAllAppsJobService.launchJob(firebaseJobDispatcher)
    }


    companion object {
        private const val TAG = "MyApp"

        var instance: MyApp? = null
        private set

        @JvmStatic fun refWatcher(context: Context): RefWatcher =
                (context.applicationContext as MyApp).refWatcher
    }
}