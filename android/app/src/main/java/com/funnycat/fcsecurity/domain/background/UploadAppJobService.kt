package com.funnycat.fcsecurity.domain.background

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import com.funnycat.fcsecurity.R
import com.funnycat.fcsecurity.data.datasources.database.AppDao
import com.funnycat.fcsecurity.data.datasources.webservice.AppWebService
import com.funnycat.fcsecurity.data.models.Status
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import com.firebase.jobdispatcher.*
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.data.models.BasicResult
import com.funnycat.fcsecurity.utils.TimeUtils
import com.funnycat.fcsecurity.ui.commons.CustomNotification
import dagger.android.AndroidInjection
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.concurrent.Executor


class UploadAppJobService : JobService() {

    @Inject
    lateinit var appDao: AppDao

    @Inject
    lateinit var webService: AppWebService

    @Inject
    lateinit var executor: Executor

    @Inject
    lateinit var sp: SharedPreferences

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }


    override fun onStopJob(job: JobParameters) = true // Answers the question: "Should this job be retried?"

    override fun onStartJob(job: JobParameters): Boolean {
        task(job).start()
        return true // Answers the question: "Is there still work going on?"
    }


    private fun task(job: JobParameters) = Thread {
        val sha256 = job.extras?.getString(ARG_SHA256)
        Log.d(TAG, "start task:: hash -> $sha256")
        if (sha256 == null) {
            Log.d(TAG, "stop task")
            jobFinished(job, false)
        } else {
            val app = appDao.getAppObjectFromHash(sha256)
            if (app == null) jobFinished(job, false)
            if (app != null && app.status == Status.UNKNOWN) {
                app.status = Status.UPLOADING
                appDao.update(app)
                val notify = job.extras?.getBoolean(ARG_NOTIFY, false)!!

                if (notify) updateNotification(app)

                val notifyS = if (notify) "1" else "0"
                val appFile = File(app.path)
                val sha256RB = RequestBody.create(MediaType.parse("multipart/form-data"), sha256)
                val fcmTokenRB = RequestBody.create(MediaType.parse("multipart/form-data"), MyFirebaseMessagingService.getFCMToken(sp))
                val notifyRB = RequestBody.create(MediaType.parse("multipart/form-data"), notifyS)
                val fileRB = RequestBody.create(MediaType.parse("multipart/form-data"), appFile)

                val file = MultipartBody.Part.createFormData("file", appFile.name, fileRB)

                webService.uploadApp(file, sha256RB, fcmTokenRB, notifyRB).enqueue(object : Callback<BasicResult> {
                    override fun onFailure(call: Call<BasicResult>?, t: Throwable?) {
                        executor.execute {
                            app.status = Status.UNKNOWN
                            appDao.update(app)
                            if (notify) updateNotification(app)
                            jobFinished(job, true)
                        }
                    }

                    override fun onResponse(call: Call<BasicResult>?, response: Response<BasicResult>?) {
                        var needsReschedule = true

                        executor.execute {
                            when (response?.code()) {
                                202 -> {
                                    if (response.body() != null) {
                                        app.status = Status.UPLOADED
                                        needsReschedule = false
                                    } else app.status = Status.UNKNOWN
                                }
                                else -> app.status = Status.UNKNOWN
                            }
                            appDao.update(app)
                            if (notify) updateNotification(app)
                            jobFinished(job, needsReschedule)
                        }
                    }
                })
            }
        }
    }

    private fun updateNotification(app: App) {
        Log.d(TAG, "updateNotification: ${app.package_name}, status: ${app.status.name}")
        val notification = CustomNotification(this, app.hash.hashCode())
        val title: String
        val contentText: String
        val progress = app.status == Status.UPLOADING
        when (app.status) {
            Status.UPLOADING  -> {
                title = baseContext.getString(R.string.title_uploading_notification)
                contentText = baseContext.getString(R.string.subtitle_uploading_notification, app.name)
            }
            Status.UPLOADED   -> {
                title = baseContext.getString(R.string.title_uploaded_notification)
                contentText = baseContext.getString(R.string.subtitle_uploaded_notification, app.name)
            }
            else            -> {
                title = baseContext.getString(R.string.title_error_notification)
                contentText = baseContext.getString(R.string.subtitle_error_uploading_notification, app.name)
            }
        }
        notification.showNotification(title = title, contentText = contentText, smallIcon = R.drawable.ic_notification_icon, runProgress = progress)
    }

    companion object {
        private const val TAG = "UploadAppJobService"

        private const val ARG_SHA256 = "sha256"
        private const val ARG_NOTIFY = "notify"

        private fun makeJob(dispatcher: FirebaseJobDispatcher, sha256: String, notify: Boolean, executeNow: Boolean) : Job {

            val windowsStart = 0
            val windowsEnd = windowsStart +
                    if (executeNow) 0 else TimeUtils.minutesToSeconds(5)

            Log.d(TAG, "next work start in: [$windowsStart - $windowsEnd]")

            val extras = Bundle().apply {
                putString(ARG_SHA256, sha256)
                putBoolean(ARG_NOTIFY, notify)
            }

            return dispatcher.newJobBuilder()
                    // the JobService that will be called
                    .setService(UploadAppJobService::class.java)
                    // uniquely identifies the job
                    .setTag("${UploadAppJobService::class.java.simpleName}::$sha256")
                    // one-off job
                    .setRecurring(false)
                    // persist forever
                    .setLifetime(Lifetime.FOREVER)
                    // start between windowsStart and windowsEnd seconds from now
                    .setTrigger(Trigger.executionWindow(windowsStart, windowsEnd))
                    // don't overwrite an existing job with the same tag
                    .setReplaceCurrent(false)
                    // retry with exponential backoff
                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                    // constraints that need to be satisfied for the job to run
                    .setConstraints(Constraint.ON_UNMETERED_NETWORK)
                    .setExtras(extras)
                    .build()
        }

        fun launchJob(dispatcher: FirebaseJobDispatcher, sha256: String, notify: Boolean = false, executeNow: Boolean = false) {
            Log.d(TAG, "programJobs")
            val job = makeJob(dispatcher, sha256, notify, executeNow)
            dispatcher.mustSchedule(job)
        }
    }

}