package com.funnycat.fcsecurity.domain.background

import android.app.PendingIntent
import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import com.funnycat.fcsecurity.R
import com.funnycat.fcsecurity.data.datasources.database.AppDao
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.data.models.AppNotifyResult
import com.funnycat.fcsecurity.data.models.Status
import com.funnycat.fcsecurity.ui.commons.CustomNotification
import com.funnycat.fcsecurity.ui.detailsapp.DetailsAppActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import dagger.android.AndroidInjection
import javax.inject.Inject

open class MyFirebaseMessagingService : FirebaseMessagingService() {

    @Inject lateinit var sp: SharedPreferences

    @Inject lateinit var appDao: AppDao

    @Inject lateinit var gson: Gson

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onNewToken(newToken: String?) {
        super.onNewToken(newToken)
        Log.d(TAG, "onNewToken")
        saveFCMToken(sp, newToken)
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        super.onMessageReceived(message)
        Log.d(TAG, "onMessageReceived")
        message?.notification?.body?.let { it ->
            Log.d(TAG, "notification body: $it")
            val appNotifyResult = convertToAppNotifyResult(it)
            val appResult = appNotifyResult.app_result
            appDao.getAppObjectFromHash(appResult.sha256)?.let { app ->
                app.status = Status.ANALIZED
                app.positive = appResult.positive
                appDao.update(app)
                showNotification(app)
            }
        }
    }

    private fun convertToAppNotifyResult(msg: String) =
            gson.fromJson(msg, AppNotifyResult::class.java)

    private fun showNotification(app: App) {
        Log.d(TAG, "updateNotification: ${app.package_name}")
        val notification = CustomNotification(this, app.hash.hashCode())
        val result = baseContext.getString(if (app.positive) R.string.malware else R.string.clean)
        val intent = DetailsAppActivity.getCallingIntent(this, app.hash)
        val pIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        notification.showNotification(title = baseContext.getString(R.string.title_result_notification),
                contentText = baseContext.getString(R.string.subtitle_result_notification, app.name, result),
                smallIcon = R.drawable.ic_notification_icon,
                pendingIntent = pIntent)
    }

    companion object {
        private const val TAG = "MyFirebaseMessagingService"

        private const val SP_FCM_TOKEN = "fcm_token"

        fun getFCMToken(sp: SharedPreferences) = sp.getString(SP_FCM_TOKEN, "")

        fun saveFCMToken(sp: SharedPreferences, token: String?) {
            token?.let {
                sp.edit { putString(SP_FCM_TOKEN, it) }
                // sendRegistrationToServer(it)
                Log.d(TAG, "onNewToken:: $it")
            }
        }
    }
}

// fbCdtzB0u54:APA91bGmKlHLrvVRynwFRwAHH82gzH9SazTgGHaYJvvk7xitcGcCPU6DVHXAkfq8IAl5jvhIpe5357BuZZ9gcsKdVK--ZWThOIJFCAMwOsn08G48tMI1HjHHWPqVbVjG7AktSz1eiOye81pPPAEW36PPrUBu9ttcnA

// {'sha256': 'f605bf059ae0fd3b8b529a7741f6cac63e0458c1f88d193c62c0cfc6004c1dc5', 'positive': True}

// {'sha256': 'f20066bcdd744cf1ec14750fc8463671671a8409514d8b424d8a18b6822cec0b', 'positive': True}
