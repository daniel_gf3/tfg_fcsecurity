package com.funnycat.fcsecurity.ui.fcsecurity

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.data.models.TypeApp
import com.funnycat.fcsecurity.domain.usercases.LoadAllAppsUseCase
import com.funnycat.fcsecurity.domain.usercases.LoadSystemAppsUseCase
import com.funnycat.fcsecurity.domain.usercases.LoadUserAppsUseCase
import com.funnycat.fcsecurity.ui.commons.ActionLiveData
import javax.inject.Inject



class FCSecurityViewModel @Inject constructor(private val loadAllAppsUseCase: LoadAllAppsUseCase,
                                              private val loadAllUserAppsUseCase: LoadUserAppsUseCase,
                                              private val loadAllSystemAppsUseCase: LoadSystemAppsUseCase) : ViewModel() {

    private var apps: LiveData<List<App>>? = null

    val toastAction = ActionLiveData<String>()

    fun getApps() = getAllAppsFromType()

    fun loadUserApps() = getAllAppsFromType(TypeApp.USER)

    fun loadSystemApps() = getAllAppsFromType(TypeApp.SYSTEM)

    private fun getAllAppsFromType(type: TypeApp? = null) : LiveData<List<App>> {
        Log.d(TAG, "Vamos a cargar: ${type?.name}")
        if (apps != null) return apps!!
        apps = when (type) {
            TypeApp.USER    -> loadAllUserAppsUseCase.execute(Unit)
            TypeApp.SYSTEM  -> loadAllSystemAppsUseCase.execute(Unit)
            else            -> loadAllAppsUseCase.execute(Unit)
        }
        return apps!!
    }

    fun onClickListener(hash: String) {
//        toastAction.sendAction(hash)
    }

    companion object {
        private const val TAG = "FCSecurityViewModel"
    }
}