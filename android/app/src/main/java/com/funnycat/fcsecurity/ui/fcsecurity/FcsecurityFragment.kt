package com.funnycat.fcsecurity.ui.fcsecurity

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.Pair
import androidx.fragment.app.Fragment

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.funnycat.fcsecurity.R
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.data.models.TypeApp
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.funnycat.fcsecurity.ui.commons.OnItemListener
import com.funnycat.fcsecurity.ui.navigateToDetailActivity
import com.funnycat.fcsecurity.utils.toast
import kotlinx.android.synthetic.main.content_fcsecurity.*


class FcsecurityFragment : Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: FCSecurityViewModel

    private lateinit var appsAdapter: AppsAdapter

    private var mTypeApp: TypeApp? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let { mTypeApp = TypeApp.values()[it.getInt(ARG_TYPE_APP)] }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.content_fcsecurity, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureDagger()
        setupAdapter()
        setupViewModel(mTypeApp)
    }

    // -----------------
    // CONFIGURATION
    // -----------------

    private fun configureDagger() {
        AndroidSupportInjection.inject(this)
    }

    private fun setupAdapter() {
        appsAdapter = AppsAdapter(listener = object : OnItemListener {
            override fun onClickListener(hash: String, vararg sharedElements: Pair<View, String>) {
                Log.d(TAG, "onClickListener:: hash: $hash")
                viewModel.onClickListener(hash)
                activity?.navigateToDetailActivity(hash, *sharedElements)
            }
        })

        rv_list_apps.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()
            adapter = appsAdapter
        }
    }

    private fun setupViewModel(type: TypeApp? = null) {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(FCSecurityViewModel::class.java)
        when (type) {
            TypeApp.USER    -> viewModel.loadUserApps()
            TypeApp.SYSTEM  -> viewModel.loadSystemApps()
            else            -> viewModel.getApps()
        }.observe(this, Observer<List<App>> { apps ->
            Log.d(TAG, "Receiving database update from LiveData")
            Log.d(TAG, "numApks: ${apps.size}")
            tv_message.visibility = if (apps.isEmpty()) View.VISIBLE else View.GONE
            appsAdapter.updateData(apps)
        })

        // Actions
        viewModel.toastAction.observe(this, Observer { msg -> msg?.let { context?.toast(it) } })

    }

    companion object {
        private const val TAG = "FcsecurityFragment"

        private const val ARG_TYPE_APP = "type_app"

        fun newInstance(typeApks: TypeApp? = null) : FcsecurityFragment {
            val fragment = FcsecurityFragment()
            if (typeApks == null) return fragment
            val args = Bundle()
            args.putInt(ARG_TYPE_APP, typeApks.ordinal)
            fragment.arguments = args
            return fragment
        }
    }
}
