package com.funnycat.fcsecurity.ui.detailsapp

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.domain.usercases.LoadAppUseCase
import com.funnycat.fcsecurity.domain.usercases.UploadAppUseCase
import javax.inject.Inject

class DetailsAppViewModel @Inject constructor(private val loadAppUseCase: LoadAppUseCase, private val uploadAppUseCase: UploadAppUseCase) : ViewModel() {

    private var app: LiveData<App?>? = null

    fun loadApp(hash: String) : LiveData<App?> {
        Log.d(TAG, "loadApp: $hash")
        if (app == null) {
            Log.d(TAG, "app == null")
            app = loadAppUseCase.execute(hash)
        }
        return app!!
    }

    fun uploadApp(hash: String) {
        Log.d(TAG, "uploadApp: $hash")
        app?.let { uploadAppUseCase.execute(hash) }
    }

    companion object {
        private const val TAG = "DetailsAppViewModel"
    }
}
