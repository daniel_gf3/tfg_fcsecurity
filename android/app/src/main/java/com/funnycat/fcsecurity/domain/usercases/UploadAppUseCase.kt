package com.funnycat.fcsecurity.domain.usercases

import com.funnycat.fcsecurity.data.repositories.AppRepository
import javax.inject.Inject

class UploadAppUseCase @Inject constructor(private val repository: AppRepository): Command<String, Unit> {
    override fun execute(parameters: String) {
        return repository.uploadApp(parameters)
    }
}