package com.funnycat.fcsecurity.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import com.funnycat.fcsecurity.data.datasources.database.AppDatabase
import com.funnycat.fcsecurity.data.datasources.system.AppSystemSource
import com.funnycat.fcsecurity.data.datasources.webservice.AppWebService
import com.funnycat.fcsecurity.data.datasources.database.AppDao
import com.funnycat.fcsecurity.data.repositories.AppRepository
import com.funnycat.fcsecurity.utils.Utils
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = app.applicationContext


    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app.baseContext)

    @Provides
    @Singleton
    fun providesUtils(): Utils = Utils(app)

    @Provides
    @Singleton
    fun providesGson(): Gson = Gson()

    @Provides
    @Singleton
    fun providesFirebaseJobDispatcher(): FirebaseJobDispatcher = FirebaseJobDispatcher(GooglePlayDriver(app))

    // --- REPOSITORY INJECTION ---

    @Provides
    fun provideExecutor(): Executor {
        return Executors.newSingleThreadExecutor()
    }

    @Provides
    @Singleton
    fun provideAppSystemSource(app: Application): AppSystemSource = AppSystemSource(app)

    @Provides
    @Singleton
    fun provideAppRepository(webService: AppWebService, appDao: AppDao, firebaseJobDispatcher: FirebaseJobDispatcher,
                             systemSource: AppSystemSource, sp: SharedPreferences, executor: Executor): AppRepository {
        return AppRepository(webService, appDao, firebaseJobDispatcher, systemSource, sp, executor)
    }

    // ------

    @Provides
    @Singleton
    fun provideAppDatabase(app: Application): AppDatabase =
            Room.databaseBuilder(app, AppDatabase::class.java, "appDatabase.db").build()

    @Provides
    @Singleton
    fun provideAppDao(db: AppDatabase): AppDao = db.getApkDao()
}