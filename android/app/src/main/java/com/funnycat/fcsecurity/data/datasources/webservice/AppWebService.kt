package com.funnycat.fcsecurity.data.datasources.webservice

import com.funnycat.fcsecurity.data.models.AppResult
import com.funnycat.fcsecurity.data.models.BasicResult
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


interface AppWebService {

    @GET("/app-result/{sha256}")
    fun getApp(@Path("sha256") sha256: String): Call<AppResult>

    @Multipart
    @POST("/upload-app/")
    fun uploadApp(@Part file: MultipartBody.Part,
                  @Part("sha256") sha256: RequestBody,
                  @Part("fcm_token") fcmToken: RequestBody,
                  @Part("notify") notify: RequestBody): Call<BasicResult>
}