package com.funnycat.fcsecurity.ui.commons

import android.view.View


interface OnItemListener {

    fun onClickListener(hash: String, vararg sharedElements: androidx.core.util.Pair<View, String>)

}