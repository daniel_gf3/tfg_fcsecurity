package com.funnycat.fcsecurity.data.datasources.database

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.funnycat.fcsecurity.data.models.App

@Dao
interface AppDao {

    @Query("SELECT COUNT(*) FROM app")
    fun numApps(): Int

    @Query("SELECT * FROM app ORDER BY positive DESC, user_app DESC, package_name")
    fun getAllLiveApps(): LiveData<List<App>>

    @Query("SELECT * FROM app ORDER BY positive DESC, user_app DESC, package_name")
    fun getAllApps(): List<App>

    @Query("SELECT * FROM app WHERE user_app=1 ORDER BY package_name")
    fun getAllLiveUserApps(): LiveData<List<App>>

    @Query("SELECT * FROM app WHERE user_app=0 ORDER BY package_name")
    fun getAllLiveSystemApps(): LiveData<List<App>>

    @Query("SELECT * FROM app WHERE status=0 ORDER BY package_name LIMIT 1")
    fun getFirstUnknownApp(): App?

    @Query("SELECT * FROM app WHERE id=:id")
    fun getLiveAppFromId(id: Int): LiveData<App?>

    @Query("SELECT * FROM app WHERE package_name=:packageName")
    fun getLiveAppFromPackageName(packageName: Int): LiveData<App?>

    @Query("SELECT * FROM app WHERE hash=:hash")
    fun getLiveAppFromHash(hash: String): LiveData<App?>

    @Query("SELECT * FROM app WHERE hash=:hash")
    fun getAppObjectFromHash(hash: String): App?

    @Insert(onConflict = REPLACE)
    fun insert(vararg app: App)

    @Update
    fun update(vararg app: App)

    @Delete
    fun delete(vararg app: App)

    @Query("DELETE FROM app")
    fun deleteAll()

    @Query("DELETE FROM app WHERE package_name=:packageName")
    fun deleteFromPackageName(packageName: String)
}