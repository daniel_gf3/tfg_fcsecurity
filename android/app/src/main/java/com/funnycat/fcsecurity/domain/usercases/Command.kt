package com.funnycat.fcsecurity.domain.usercases

internal interface Command<in P, R> {
    fun execute(parameters: P): R
}