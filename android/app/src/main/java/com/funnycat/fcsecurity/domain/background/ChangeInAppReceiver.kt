package com.funnycat.fcsecurity.domain.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import com.funnycat.fcsecurity.data.repositories.AppRepository

class ChangeInAppReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean(AppRepository.SP_ANALYSIS_COMPLETED, false)) {
            with (intent) {
                if (dataString != null) {
                    val packageName = dataString.split(":")[1]
                    when (action) {
                        Intent.ACTION_PACKAGE_ADDED     -> ChangeInAppService.startActionAdded(context, packageName)
                        Intent.ACTION_PACKAGE_REMOVED   -> ChangeInAppService.startActionRemoved(context, packageName)
                    }
                }
            }
        }
    }

    companion object {
        private const val TAG = "ChangeInAppReceiver"
    }
}
