package com.funnycat.fcsecurity.domain.background

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.util.Log
import com.funnycat.fcsecurity.data.datasources.database.AppDao
import com.funnycat.fcsecurity.data.models.AppDataMapper
import com.funnycat.fcsecurity.utils.PackageUtils
import dagger.android.AndroidInjection
import javax.inject.Inject

private const val ACTION_ADDED = "com.funnycat.fcsecurity.logic.background.action.ADDED"
private const val ACTION_REMOVED = "com.funnycat.fcsecurity.logic.background.action.REMOVED"

private const val EXTRA_PACKAGE_NAME = "com.funnycat.fcsecurity.logic.background.extra.PACKAGE_NAME"

/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
class ChangeInAppService : IntentService("ChangeInAppService") {

    @Inject
    lateinit var appDao: AppDao

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onHandleIntent(intent: Intent?) {
        val packageName = intent?.getStringExtra(EXTRA_PACKAGE_NAME)
        when (intent?.action) {
            ACTION_ADDED ->     handleActionAdded(packageName!!)
            ACTION_REMOVED ->   handleActionRemoved(packageName!!)
        }
    }

    /**
     * Handle action Added in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionAdded(packageName: String) {
        Log.d(TAG, "handleActionAdded")
        PackageUtils.calculateApkHash(packageManager, packageName)?.let {
            val app = AppDataMapper().convertToDomain(this, it)
            appDao.insert(app)
            Log.d(TAG, "inserted new app: $packageName")
        }
    }

    /**
     * Handle action Removed in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionRemoved(packageName: String) {
        Log.d(TAG, "handleActionRemoved")
        appDao.deleteFromPackageName(packageName)
    }

    companion object {

        private const val TAG = "ChangeInAppService"

        /**
         * Starts this service to perform action Added with the given parameters. If
         * the service is already performing a task this action will be queued.
         *
         * @see IntentService
         */
        @JvmStatic
        fun startActionAdded(context: Context, packageName: String) {
            val intent = Intent(context, ChangeInAppService::class.java).apply {
                action = ACTION_ADDED
                putExtra(EXTRA_PACKAGE_NAME, packageName)
            }
            context.startService(intent)
        }

        /**
         * Starts this service to perform action Removed with the given parameters. If
         * the service is already performing a task this action will be queued.
         *
         * @see IntentService
         */
        @JvmStatic
        fun startActionRemoved(context: Context, packageName: String) {
            val intent = Intent(context, ChangeInAppService::class.java).apply {
                action = ACTION_REMOVED
                putExtra(EXTRA_PACKAGE_NAME, packageName)
            }
            context.startService(intent)
        }
    }
}
