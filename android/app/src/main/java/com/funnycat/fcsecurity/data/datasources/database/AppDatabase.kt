package com.funnycat.fcsecurity.data.datasources.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.funnycat.fcsecurity.data.datasources.database.converters.StatusConverter
import com.funnycat.fcsecurity.data.models.App

@Database(entities = [App::class], version = 1)
@TypeConverters(StatusConverter::class)

abstract class AppDatabase : RoomDatabase() {


    abstract fun getApkDao() : AppDao

//    companion object {
//        private const val DB_NAME = "appDatabase.db"
//
//        private var INSTANCE: AppDatabase? = null
//
//        fun getInstance(context: Context): AppDatabase? {
//            if (INSTANCE == null) {
//                synchronized(AppDatabase::class) {
//                    INSTANCE = Room.databaseBuilder(
//                            context.applicationContext,
//                            AppDatabase::class.java,
//                            DB_NAME)
//                            .build()
//                }
//            }
//            return INSTANCE
//        }
//
//        fun destroyInstance() {
//            INSTANCE = null
//        }
//    }
}