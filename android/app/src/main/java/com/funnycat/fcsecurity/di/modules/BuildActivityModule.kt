package com.funnycat.fcsecurity.di.modules

import com.funnycat.fcsecurity.ui.detailsapp.DetailsAppActivity
import com.funnycat.fcsecurity.ui.fcsecurity.FCSecurityActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildActivityModule {
    @ContributesAndroidInjector(modules = [BuildFragmentModule::class])
    abstract fun contributeFCSecurityActivity() : FCSecurityActivity

    @ContributesAndroidInjector(modules = [BuildFragmentModule::class])
    abstract fun contributeDetailsAppActivity() : DetailsAppActivity
}