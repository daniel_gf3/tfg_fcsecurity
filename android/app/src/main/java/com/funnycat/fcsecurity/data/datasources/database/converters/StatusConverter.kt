package com.funnycat.fcsecurity.data.datasources.database.converters

import androidx.room.TypeConverter
import com.funnycat.fcsecurity.data.models.Status

class StatusConverter {
    @TypeConverter
    fun IntToStatus(status: Int) = Status.values()[status]

    @TypeConverter
    fun statusToInt(status: Status) = status.ordinal
}