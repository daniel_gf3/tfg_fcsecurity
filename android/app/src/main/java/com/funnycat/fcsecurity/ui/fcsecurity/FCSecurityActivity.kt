package com.funnycat.fcsecurity.ui.fcsecurity

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import com.funnycat.fcsecurity.R
import com.funnycat.fcsecurity.ui.AbstractActivity
import dagger.android.AndroidInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject
import dagger.android.DispatchingAndroidInjector


class FCSecurityActivity : AbstractActivity(), HasSupportFragmentInjector {

    @Inject lateinit var mFragmentInjector: DispatchingAndroidInjector<Fragment>


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fcsecurity_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.transaction(allowStateLoss = true) {
                replace(R.id.container, FcsecurityFragment.newInstance())
            }

        }
    }

    override fun supportFragmentInjector() = mFragmentInjector
}
