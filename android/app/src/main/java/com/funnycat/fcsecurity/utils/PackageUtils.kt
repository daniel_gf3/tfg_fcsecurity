package com.funnycat.fcsecurity.utils

import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.PermissionInfo
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.util.SparseArray
import com.funnycat.fcsecurity.data.models.TypeApp
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.util.concurrent.Semaphore

object PackageUtils {


    fun getPackageArchiveInfo(pm: PackageManager, path: String): PackageInfo?{
        val flags = PackageManager.GET_ACTIVITIES or PackageManager.GET_CONFIGURATIONS or PackageManager.GET_GIDS or
                PackageManager.GET_INSTRUMENTATION or PackageManager.GET_INTENT_FILTERS or PackageManager.GET_META_DATA or
                PackageManager.GET_PERMISSIONS or PackageManager.GET_PROVIDERS or PackageManager.GET_RECEIVERS or
                PackageManager.GET_RESOLVED_FILTER or PackageManager.GET_SERVICES or PackageManager.GET_SHARED_LIBRARY_FILES or
                PackageManager.GET_SIGNATURES or PackageManager.GET_URI_PERMISSION_PATTERNS
        Log.d("getPackageArchiveInfo", "All Flags = $flags")
        return getPackageArchiveInfo(pm, path, flags)
    }


    fun getPackageArchiveInfo(pm: PackageManager, path: String, flags: Int) = try{
        pm.getPackageArchiveInfo(path, flags)
    }catch (e: Exception){
        e.printStackTrace()
        null
    }

//    fun getSignStatus(appInfo: ApplicationInfo) = ApkFile(appInfo.sourceDir).verifyApk()
//
//    fun getAplicationNameFromApkFilePackage(apkFile: ApkFile) = apkFile.apkMeta.label
//
//    fun getAplicationNameFromApkFilePackage(path: String) = getAplicationNameFromApkFilePackage(ApkFile(File(path)))

    fun getPackageNameFromPath(pm: PackageManager, path: String) = try{ pm.getPackageArchiveInfo(path, 0).packageName } catch (e: Exception) { null }

    fun getApplicationInfo(pm: PackageManager, pck: String) = try{ pm.getApplicationInfo(pck, 0) }catch (e: Exception){ null }

    fun getIconFromApplicationInfo(pm: PackageManager, appInfo: ApplicationInfo): Bitmap? {
        return if (appInfo.loadLogo(pm) != null) appInfo.loadLogo(pm).getBitmap()
        else appInfo.loadIcon(pm).getBitmap()
    }

    fun getIconFromPackage(pm: PackageManager, pck: String): Bitmap? {
        val appInfo = getApplicationInfo(pm, pck) ?: return null
        return if (appInfo.loadLogo(pm) != null) appInfo.loadLogo(pm).getBitmap()
        else appInfo.loadIcon(pm).getBitmap()
    }

    fun getLogo(context: Context, packageName: String) = try {
        with(PackageUtils.getApplicationInfo(context.packageManager, packageName)) {
            this!!.loadLogo(context.packageManager) ?: loadIcon(context.packageManager)
        }
    } catch (e: Exception) { null }


    fun getPermissionInfo(pm: PackageManager, permission: String): PermissionInfo?{
        return try {
            pm.getPermissionInfo(permission, PackageManager.GET_META_DATA)
        }catch (e: PackageManager.NameNotFoundException){
            null
        }
    }

//    fun protectionLevelToColor(level: Int?) =
//            if(level== null) R.color.black_translucent
//            else when(level and PermissionInfo.PROTECTION_MASK_BASE){
//                PermissionInfo.PROTECTION_DANGEROUS -> R.color.infected_color
//                PermissionInfo.PROTECTION_SIGNATURE -> {
//                    if(level and PermissionInfo.PROTECTION_FLAG_VERIFIER != 0) R.color.suspicious_color
//                    else R.color.black_translucent
//                }
//                else    -> R.color.black_translucent
//            }

//    fun protectionLevelToString(level: Int): String? {
//        var protLevel: String? = null
//        when (level and PermissionInfo.PROTECTION_MASK_BASE) {
//            PermissionInfo.PROTECTION_DANGEROUS -> protLevel = "dangerous"
//            PermissionInfo.PROTECTION_NORMAL -> protLevel = "normal"
//            PermissionInfo.PROTECTION_SIGNATURE -> protLevel = "signature"
//            PermissionInfo.PROTECTION_SIGNATURE_OR_SYSTEM -> protLevel = "signatureOrSystem"
//        }
//        if (level and PermissionInfo.PROTECTION_FLAG_PRIVILEGED != 0) {
//            protLevel += "|privileged"
//        }
//        if (level and PermissionInfo.PROTECTION_FLAG_DEVELOPMENT != 0) {
//            protLevel += "|development"
//        }
//        if (level and PermissionInfo.PROTECTION_FLAG_APPOP != 0) {
//            protLevel += "|appop"
//        }
//        if (level and PermissionInfo.PROTECTION_FLAG_PRE23 != 0) {
//            protLevel += "|pre23"
//        }
//        if (level and PermissionInfo.PROTECTION_FLAG_INSTALLER != 0) {
//            protLevel += "|installer"
//        }
//        if (level and PermissionInfo.PROTECTION_FLAG_VERIFIER != 0) {
//            protLevel += "|verifier"
//        }
//        if (level and PermissionInfo.PROTECTION_FLAG_PREINSTALLED != 0) {
//            protLevel += "|preinstalled"
//        }
//        if (level and PermissionInfo.PROTECTION_FLAG_SETUP != 0) {
//            protLevel += "|setup"
//        }
//        return protLevel
//    }

    fun getUserPermissionsFromPackage(pm: PackageManager, packageApk: String) =
            pm.getPackageInfo(packageApk, PackageManager.GET_PERMISSIONS).requestedPermissions?.asList()

    fun getPermissionsFromPackage(pm: PackageManager, packageApk: String) =
            pm.getPackageInfo(packageApk, PackageManager.GET_PERMISSIONS).permissions?.map { it.name }

    fun getAllPermissionString(pm: PackageManager, packageApk: String): List<String>{
        val tempList = ArrayList<String>()
        val userPermission = getUserPermissionsFromPackage(pm, packageApk)
        val permission = getPermissionsFromPackage(pm, packageApk)
        if(userPermission!=null) tempList.addAll(userPermission)
        if(permission!=null) tempList.addAll(permission)
        return tempList
    }

    fun getAllApks(pm: PackageManager): List<ApplicationInfo> {
        return try {
            pm.getInstalledApplications(0)
        } catch(ex: Exception) {
            getInstalledApplicationsBF(pm)
        }
    }

    private fun getInstalledApplicationsBF(pm: PackageManager): List<ApplicationInfo> {
        val result = ArrayList<ApplicationInfo>()
        var bR: BufferedReader? = null
        try {
            val process = Runtime.getRuntime().exec("pm list packages")
            bR = BufferedReader(InputStreamReader(process.inputStream))
            var line = bR.readLine()
            while (line != null) {
                val packageName = line.substring(line.indexOf(':') + 1)
                val appInfo = getApplicationInfo(pm, packageName)
                if(appInfo != null) result.add(appInfo)
                line = bR.readLine()
            }
            process.waitFor()
        } catch(e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                bR?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return result
    }

    fun hasActivitiesForGetFiles_intent(pm: PackageManager): Pair<Boolean, Intent>{
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setDataAndType(Uri.parse(Environment.getExternalStorageDirectory().toString()), "file/*")
        return Pair(getActivitiesForIntent(pm, intent).isNotEmpty(), intent)
    }

    fun getActivitiesForIntent(pm: PackageManager, intent: Intent) = pm.queryIntentActivities(intent, PackageManager.GET_ACTIVITIES)


    fun getLaunchIntentForPackage(pm: PackageManager, packageApk: String): Intent? {
        val intentShearch = Intent("android.intent.action.MAIN").addCategory("android.intent.category.LEANBACK_LAUNCHER")
        val resolver = pm.queryIntentActivities(intentShearch, 0).filter { resol -> resol.activityInfo.packageName == packageApk }
        return if (!resolver.isEmpty()) Intent().setClassName(packageApk, resolver[0].activityInfo.name).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        else null
    }

    fun getResolversIntent(pm: PackageManager, packageApk: String, action: String) =
            pm.queryBroadcastReceivers(Intent(action), 0)
                    .any { rInfo -> rInfo.toString().split(" ")[1].split("/")[0] == packageApk }


    fun exitsPackage(context: Context, packageApk: String): Boolean {
        return try {
            getApplicationInfo(context.packageManager, packageApk)!!.publicSourceDir
            true
        } catch(e: Exception) {
            e.printStackTrace()
            false
        }
    }

    fun isVirusTotalAPP(context: Context, path: String): Boolean {
        return try {
            val p = context.packageManager.getPackageInfo(context.packageName, 0)
            path == p.applicationInfo.sourceDir
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            false
        }

    }

    fun getIntentUninstallApk(context: Context, packageApk: String): Intent? {
        return if (exitsPackage(context, packageApk)) {
            Intent(Intent.ACTION_DELETE, Uri.fromParts("package", packageApk, null)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        } else null
    }

    fun getInstallerPackageName(pm: PackageManager, packageApk: String): String? = pm.getInstallerPackageName(packageApk)

//    fun getInstallerName(installerPackageName: String, apkType: ApkType) = when(installerPackageName){
//        "com.android.vending"   -> "Google Play Store"
//        "com.amazon.venezia"    -> "Amazon App Store"
//        "com.xiaomi.market"     -> "MIUI App Store"
//        ""                      -> if(apkType == ApkType.USER) "Installed manually for user" else "Installed for System"
//        else                    -> "Unknown"
//    }

    fun calculateAllApkHashes(pm: PackageManager) = getAllApks(pm).map { Pair(HashGeneratorUtils.generateSHA256F(it.sourceDir), it) }


    fun calculateAllApkHashesFast(pm: PackageManager): List<Pair<String, ApplicationInfo>>{
        val allApks = getAllApks(pm)
        val numOfCores = Runtime.getRuntime().availableProcessors()
        val semaphore = Semaphore(numOfCores)
        val numApksCore = (allApks.size/numOfCores)

        val totalList = SparseArray<List<Pair<String, ApplicationInfo>>>()
        for(i in 0 until numOfCores){
            semaphore.acquire()
            Thread(Runnable {
                val init = i * numApksCore
                val end = if(i != numOfCores-1) init + numApksCore else allApks.size
                totalList.put(i, allApks.subList(init, end).map { Pair(HashGeneratorUtils.generateSHA256F(it.sourceDir), it) })
                semaphore.release()
            }).start()
        }
        for(i in 0 until numOfCores) semaphore.acquire()
        val result = ArrayList<Pair<String, ApplicationInfo>>()
        for(i in 0 until totalList.size()) result.addAll(totalList[i])
        return result
    }


    fun calculateAPKHashes(listApks: List<ApplicationInfo>) = listApks.map { Pair(HashGeneratorUtils.generateSHA256F(it.sourceDir), it) }

    fun getUpdatedOrNewApks(pm: PackageManager, updatedDate: Long) = getAllApks(pm).filter { File(it.sourceDir).lastModified() > updatedDate }

    fun calculateApkHash(pm: PackageManager, pck: String) = getApplicationInfo(pm, pck)?.let { Pair(HashGeneratorUtils.generateSHA256F(it.sourceDir), it) }

//    fun checkPlayServices(context: Context): Boolean {
//        val resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context)
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) GooglePlayServicesUtil.getErrorDialog(resultCode, context as Activity, 9000).show()
//            else LogVT.i("Developer", "This device is not supported.")
//            return false
//        }
//        return true
//    }

    fun getTypeApp(appInfo: ApplicationInfo) = getTypeApp(appInfo.flags)

    fun getTypeApp(flags: Int): TypeApp {
        return when(flags and ApplicationInfo.FLAG_SYSTEM){
            TypeApp.SYSTEM.ordinal  -> TypeApp.SYSTEM
            else                    -> TypeApp.USER
        }
    }
}