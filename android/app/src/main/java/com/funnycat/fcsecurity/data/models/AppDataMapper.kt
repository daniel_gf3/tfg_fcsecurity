package com.funnycat.fcsecurity.data.models

import android.content.Context
import android.content.pm.ApplicationInfo
import com.funnycat.fcsecurity.utils.PackageUtils
import com.funnycat.fcsecurity.utils.UnitUtils

class AppDataMapper {

    fun convertToDomain(context: Context, pairHashApps : List<Pair<String, ApplicationInfo>>) =
            pairHashApps.map { convertToDomain(context, it) }

    fun convertToDomain(context: Context, pairHashApp : Pair<String, ApplicationInfo>): App {
        val (hash, appInfo) = pairHashApp
        return App(name = appInfo.loadLabel(context.packageManager).toString(),
                package_name = appInfo.packageName,
                path =  appInfo.sourceDir,
                user_app = PackageUtils.getTypeApp(appInfo) == TypeApp.USER,
                size = UnitUtils.getStringSize(appInfo.sourceDir),
                hash = hash,
                status = Status.UNKNOWN,
                positive = false)
    }
}