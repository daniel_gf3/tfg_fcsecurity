package com.funnycat.fcsecurity.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable

fun Bitmap.getDrawable() = BitmapDrawable(this)

fun Drawable.getBitmap(): Bitmap? {
    if (intrinsicHeight <= 0 || intrinsicWidth <= 0) return null
    return if (this is BitmapDrawable) bitmap
    else {
        val bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        setBounds(0, 0, canvas.width, canvas.height)
        draw(canvas)
        bitmap
    }
}

fun Drawable.centerCrop() = getBitmap()?.centerCrop()

fun Bitmap.centerCrop(): Bitmap {
    val length = Math.min(width, height)
    return Bitmap.createBitmap(this, (width - length) / 2, (height - length) / 2, length, length)
}

fun Context.dpFromPx(px: Float) = px / resources.displayMetrics.density

fun Context.pxFromDp(dp: Float) = dp * resources.displayMetrics.density