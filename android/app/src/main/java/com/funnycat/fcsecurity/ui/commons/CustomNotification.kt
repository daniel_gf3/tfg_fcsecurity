package com.funnycat.fcsecurity.ui.commons

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import android.app.NotificationChannel
import com.funnycat.fcsecurity.R


/**
 * Created by daniel on 05/09/16.
 */
class CustomNotification (val context: Context, val id: Int){

    val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    var listener: OnCancelListener?=null

    fun showNotification(title: String, contentText: String, smallIcon: Int?=null, runProgress: Boolean=false,
                                 soundAndVibrate: Boolean=false, cancelable: Boolean=false,  pendingIntent: PendingIntent?=null) {
        val mBuilder = NotificationCompat.Builder(context, CHANNEL_ID)

        mBuilder.setContentTitle(title)
                .setContentText(contentText)

        smallIcon?.let { mBuilder.setSmallIcon(smallIcon) }?: mBuilder.setSmallIcon(R.drawable.ic_notification_icon)

        if (soundAndVibrate/* && AllSharedPreferences.getBooleanValue(SharedKey.NOTIFICATIONS_NEW_MESSAGE_SOUND_AND_VIBRATE)*/)
            mBuilder.setDefaults(Notification.DEFAULT_ALL)
        else {
            mBuilder.setDefaults(Notification.DEFAULT_LIGHTS)
        }
        mBuilder.setProgress(0, 0, runProgress)

//        if(cancelable) {
//            //Add cancel action
//            val cancelReceive = Intent()
//            cancelReceive.action = CANCEL_ACTION
//            cancelReceive.putExtra("tag", id)
//            val pendingIntentCancel = PendingIntent.getBroadcast(context, id, cancelReceive, PendingIntent.FLAG_UPDATE_CURRENT)
//            mBuilder.addAction(R.drawable.ic_close, context.getString(R.string.cancel), pendingIntentCancel)
//        }

        pendingIntent?.let { mBuilder.setContentIntent(pendingIntent) }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(CHANNEL_ID, name, IMPORTANCE)
            mNotificationManager.createNotificationChannel(mChannel)
        }


        mNotificationManager.notify(id, mBuilder.build().apply {
            flags = if (runProgress) Notification.FLAG_NO_CLEAR else Notification.FLAG_AUTO_CANCEL
        })
    }

    fun setOnCancelListener(listener: OnCancelListener){ this.listener = listener }

    // Broadcast
//    private inner class NotificationButtonReceiver : BroadcastReceiver() {
//
//        internal var TAGBroadcast = "VirusTotalChangeReceiver"
//
//        override fun onReceive(context: Context, intent: Intent) {
//            Log.d(TAG, "Recibimos algo en el broadcast")
//            val b = intent.extras
//            b.putString("action", intent.action)
//            val msg = Message()
//            msg.data = b
//            handler.sendMessage(msg)
//        }
//
//        private val handler = @SuppressLint("HandlerLeak")
//        object : Handler() {
//
//            override fun handleMessage(msg: Message) {
//
//                when (msg.data.getString("action")) {
//                    CANCEL_ACTION -> {
//                        Log.d(TAGBroadcast, "CancelAction")
//                        val tag = msg.data.getInt("tag")
//                        mNotificationManager.cancel(tag)
//                        listener?.cancel()
//                    }
//                }
//            }
//        }
//    }




    interface OnCancelListener{
        fun cancel()
    }


    companion object {
        private const val TAG = "CustomNotification"
        const val CANCEL_ACTION = "com.funnycat.fcsecurity.logic.connectivity.services.cancel"

        private const val name = "CHANNEL_01"// The user-visible name of the channel.
        private const val CHANNEL_ID = "my_channel_01"// The id of the channel.
        private const val IMPORTANCE = NotificationManager.IMPORTANCE_HIGH
    }
}