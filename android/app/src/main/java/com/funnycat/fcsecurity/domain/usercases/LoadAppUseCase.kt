package com.funnycat.fcsecurity.domain.usercases

import androidx.lifecycle.LiveData
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.data.repositories.AppRepository
import javax.inject.Inject

class LoadAppUseCase @Inject constructor(private val repository: AppRepository) : Command<String, LiveData<App?>> {

    override fun execute(parameters: String): LiveData<App?> {
        return repository.getApp(parameters)
    }
}