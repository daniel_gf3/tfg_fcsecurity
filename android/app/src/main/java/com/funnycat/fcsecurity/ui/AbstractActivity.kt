package com.funnycat.fcsecurity.ui

import androidx.appcompat.app.AppCompatActivity
import com.funnycat.fcsecurity.di.MyApp

abstract class AbstractActivity : AppCompatActivity() {

    override fun onDestroy() {
        super.onDestroy()
        val refWatcher = MyApp.refWatcher(this)
        refWatcher.watch(this)
    }
}