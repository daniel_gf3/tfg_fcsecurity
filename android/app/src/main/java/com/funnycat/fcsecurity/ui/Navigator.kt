package com.funnycat.fcsecurity.ui

import android.app.Activity
import androidx.core.util.Pair
import android.os.Build
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import com.funnycat.fcsecurity.ui.detailsapp.DetailsAppActivity

fun Activity.navigateToDetailActivity(hash: String, vararg sharedElements: Pair<View, String>) {
    val intentToLaunch = DetailsAppActivity.getCallingIntent(baseContext, hash)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && sharedElements.isNotEmpty()) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, *sharedElements)
        startActivity(intentToLaunch, options.toBundle())
    } else {
        startActivity(intentToLaunch)
    }
}