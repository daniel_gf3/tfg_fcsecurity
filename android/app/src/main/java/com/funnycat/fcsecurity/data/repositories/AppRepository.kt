package com.funnycat.fcsecurity.data.repositories

import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.funnycat.fcsecurity.data.datasources.system.AppSystemSource
import com.funnycat.fcsecurity.data.datasources.webservice.AppWebService
import com.funnycat.fcsecurity.data.models.App
import com.funnycat.fcsecurity.data.datasources.database.AppDao
import com.funnycat.fcsecurity.data.models.AppResult
import com.funnycat.fcsecurity.data.models.Status
import com.funnycat.fcsecurity.domain.background.RefreshAppsJobService
import com.funnycat.fcsecurity.domain.background.UploadAppJobService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepository @Inject constructor(private val webService: AppWebService,
                                        private val appDao: AppDao,
                                        private val firebaseJobDispatcher: FirebaseJobDispatcher,
                                        private val systemSource: AppSystemSource,
                                        private val sp: SharedPreferences,
                                        private val executor: Executor) {


    fun getAllApps() : LiveData<List<App>> {
        refreshApps()
        return appDao.getAllLiveApps()
    }

    fun getSystemApps() : LiveData<List<App>> {
        refreshApps()
        return appDao.getAllLiveSystemApps()
    }

    fun getUserApps() : LiveData<List<App>> {
        refreshApps()
        return appDao.getAllLiveSystemApps()
    }

    fun getApp(sha256: String) : LiveData<App?> {
        refreshApp(sha256)
        return appDao.getLiveAppFromHash(sha256)
    }

    fun uploadApp(sha256: String) {
        UploadAppJobService.launchJob(firebaseJobDispatcher, sha256, notify = true, executeNow = true)
    }

    private fun refreshApps() {
        Log.d(TAG, "refreshApps")
        executor.execute { RefreshAppsJobService.launchJob(firebaseJobDispatcher) }
    }

    private fun refreshApp(sha256: String) {
        executor.execute {
            // running in a background thread
            val app = appDao.getAppObjectFromHash(sha256)
            if (app?.status != Status.ANALIZED) {
                // refresh the data
                webService.getApp(sha256).enqueue(object : Callback<AppResult> {
                    override fun onFailure(call: Call<AppResult>?, t: Throwable?) { }

                    override fun onResponse(call: Call<AppResult>?, response: Response<AppResult>?) {
                        Log.d(TAG, "DATA REFRESHED FROM NETWORK")
                        Log.d(TAG, "getApp::onResponse:: ${response?.body()}")
                        executor.execute {
                            response?.body()?.let {
                                app!!.status = Status.ANALIZED
                                app.positive = it.positive
                                appDao.update(app)
                            }
                        }
                    }
                })
            }
        }
    }

    companion object {
        private const val TAG = "AppRepository"

        const val SP_ANALYSIS_COMPLETED = "analysis_completed"
    }
}


/*
https://proandroiddev.com/the-missing-google-sample-of-android-architecture-components-guide-c7d6e7306b8f
https://developer.android.com/jetpack/docs/guide
https://android.jlelse.eu/new-android-architecture-components-in-action-creating-a-restaurants-map-application-with-room-b6c7096d80d5
 */