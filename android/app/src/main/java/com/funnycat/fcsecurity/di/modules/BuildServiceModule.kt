package com.funnycat.fcsecurity.di.modules

import com.funnycat.fcsecurity.domain.background.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildServiceModule {

    @ContributesAndroidInjector
    abstract fun contributeChangeInAppService() : ChangeInAppService

    @ContributesAndroidInjector
    abstract fun contributeMyFirebaseMessagingService() : MyFirebaseMessagingService

    @ContributesAndroidInjector
    abstract fun contributeUploadAppJobService() : UploadAppJobService

    @ContributesAndroidInjector
    abstract fun contributeUploadAllAppsJobService() : UploadAllAppsJobService


    @ContributesAndroidInjector
    abstract fun contributeRefreshAppsJobService() : RefreshAppsJobService
}