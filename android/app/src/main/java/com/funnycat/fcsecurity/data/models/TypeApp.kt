package com.funnycat.fcsecurity.data.models

enum class TypeApp {
    USER, SYSTEM
}