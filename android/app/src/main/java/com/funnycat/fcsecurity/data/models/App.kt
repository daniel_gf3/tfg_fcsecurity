package com.funnycat.fcsecurity.data.models

import android.graphics.drawable.Drawable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Ignore


@Entity(tableName = "app")
data class App (@PrimaryKey(autoGenerate = true)
           var id: Long = 0L,
           var name: String? = "",
           var package_name: String = "",
           var path: String = "",
           var user_app: Boolean = false,
           var size: String = "",
           var hash: String = "",
           var status: Status = Status.UNKNOWN,
           var positive: Boolean = false,
           @Ignore var icon: Drawable? = null)

enum class Status {
    UNKNOWN, UPLOADING, UPLOADED, ANALIZED
}