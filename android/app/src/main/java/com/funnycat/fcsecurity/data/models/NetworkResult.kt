package com.funnycat.fcsecurity.data.models

data class BasicResult(val sha256: String = "", val result: String)

data class AppResult(val sha256: String, val positive: Boolean)

data class AppNotifyResult(val app_result: AppResult, val notify: Boolean)