#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 16:44:17 2018

@author: daniel
"""

from backend.ai.ann import ANN
from backend.ai.cluster import Cluster


def analyze_v2(samples, threshold):
    return analyze_v3(samples, 0.0, threshold)


def analyze_v3(samples, min_threshold=0.2, max_threshold=0.3):
    ann = ANN()
    ann_pd = ann.analyze(samples)

    need_cluster = []
    for i in range(len(samples)):
        if min_threshold < ann_pd[i] < max_threshold:
            need_cluster.append(samples[i])

    if len(need_cluster) == 0:
        return ann_pd

    cluster = Cluster()
    clusters_pd = cluster.analyze(need_cluster)
    result = []
    for i in range(len(samples)):
        if min_threshold < ann_pd[i] < max_threshold:
            c_pd = clusters_pd.pop(0)
            if c_pd != -1:
                result.append(c_pd)
            else:
                result.append(ann_pd[i])
        else:
            result.append(ann_pd[i])

    return result


def analyze(samples, threshold=0.3):
    return analyze_v2(samples, threshold)


def is_malware(sample, threshold=0.3):
    if ANN().is_malware(sample) > threshold:
        return True
    if Cluster().analyze([sample])[0] == 1:
        return True
    return False


def predict(samples, threshold=0.3):
    import numpy as np
    result = np.array(analyze(samples, threshold))
    return result > threshold
