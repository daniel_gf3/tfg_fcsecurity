import itertools
# import matplotlib.pyplot as plt
import numpy as np


# #############################################################################
# Build Classifier
def build_classifier(optimizer='adam', learn_rate=1, init='uniform', rate=0.2):
    from keras.models import Sequential
    from keras.layers import Dense, Dropout
    from keras.optimizers import Adam, RMSprop
    if optimizer == 'adam':
        optimizer = Adam(lr=learn_rate)
    else:
        optimizer = RMSprop(lr=learn_rate)

    # Create classifier
    classifier = Sequential()

    classifier.add(Dense(500, kernel_initializer='uniform', activation='relu', input_dim=414))
    classifier.add(Dropout(rate=rate))
    classifier.add(Dense(500, activation='relu', kernel_initializer=init))
    classifier.add(Dropout(rate=rate))
    classifier.add(Dense(300, activation='relu', kernel_initializer=init))
    classifier.add(Dropout(rate=rate))
    classifier.add(Dense(1, activation='sigmoid', kernel_initializer=init))

    # Compile classifier
    classifier.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy', ])
    return classifier


# #############################################################################
# https://machinelearningmastery.com/save-load-keras-deep-learning-models/
# Saving ANN Model
def save_classifier(classifier, model_name='model.yaml', weights_name='model.h5'):
    # serialize model to YAML
    model_yaml = classifier.to_yaml()
    with open(model_name, "w") as yaml_file:
        yaml_file.write(model_yaml)
    # serialize weights to HDF5
    classifier.save_weights(weights_name)
    print("Saved model to disk")


# #############################################################################
# Load ANN Model
def load_classifier(model_name='model.yaml', weights_name='model.h5'):
    from keras.models import model_from_yaml
    # load YAML and create model
    yaml_file = open(model_name, 'r')
    loaded_model_yaml = yaml_file.read()
    yaml_file.close()
    loaded_model = model_from_yaml(loaded_model_yaml)
    # load weights into new model
    loaded_model.load_weights(weights_name)
    print("Loaded model from disk")
    return loaded_model


# #############################################################################
# #############################################################################
# http://scikit-learn.org/stable/modules/model_persistence.html
def save_model(model, name):
    from sklearn.externals import joblib
    joblib.dump(model, name)


def load_model(name):
    from sklearn.externals import joblib
    return joblib.load(name)


# Saving Scaler
def save_scaler(clf, name='sc.pkl'):
    save_model(clf, name)


# #############################################################################
# Load Scaler
def load_scaler(name='sc.pkl'):
    return load_model(name)


# #############################################################################

# Saving Model
def save_cluster(cluster, name):
    save_model(cluster, name)


# #############################################################################
# Load Scaler
def load_cluster(name):
    return load_model(name)


# #############################################################################
# #############################################################################
# Making the Confusion Matrix
'''
def plot_cm(cm, classes,
            normalize=False,
            title='Confusion matrix',
            cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def plot_confusion_matrix(y_pred, y_test, thresholds=[0.5, ]):
    from sklearn.metrics import confusion_matrix

    # evaluate the model
    for i in thresholds:
        y_pred_custom = (y_pred > i)
        print('pred > ', i)
        cnf_matrix = confusion_matrix(y_test, y_pred_custom)

        class_names = ['goodware', 'malware']

        # Plot non-normalized confusion matrix
        plt.figure()
        plot_cm(cnf_matrix, classes=class_names,
                title='Confusion matrix, without normalization')

        # Plot normalized confusion matrix
        plt.figure()
        plot_cm(cnf_matrix, classes=class_names, normalize=True,
                title='Normalized confusion matrix')

        plt.show()
'''

# #############################################################################
# #############################################################################
# Cluster

def generate_clusters(X_train, min_cluster_size, min_samples,
                      metric="euclidean", plot_outliers=False):
    import hdbscan
    #    %matplotlib inline

    clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size,
                                min_samples=min_samples, metric=metric,
                                gen_min_span_tree=True,
                                allow_single_cluster=True).fit(X_train)

    # Predict cluster
    clusterer.generate_prediction_data()
    return clusterer


def evaluate_cluster(X_goodware_test, X_malware_test, clusterer, type_):
    import hdbscan
    # Calculate acc
    test_labels_gw, strengths_gw = hdbscan.approximate_predict(clusterer, X_goodware_test)
    total_gw = len(X_goodware_test)

    if type_ == 'goodware':
        success_gw = len([l for l in test_labels_gw if l != -1])
    else:
        success_gw = len([l for l in test_labels_gw if l == -1])
    acc_gw = (success_gw * 100) / total_gw

    test_labels_mw, strengths_mw = hdbscan.approximate_predict(clusterer, X_malware_test)
    total_mw = len(X_malware_test)
    if type_ == 'goodware':
        success_mw = len([l for l in test_labels_mw if l == -1])
    else:
        success_mw = len([l for l in test_labels_mw if l != -1])
    acc_mw = (success_mw * 100) / total_mw

    success_total = success_gw + success_mw
    acc_total = (success_total * 100) / (total_gw + total_mw)

    return acc_gw, acc_mw, acc_total
