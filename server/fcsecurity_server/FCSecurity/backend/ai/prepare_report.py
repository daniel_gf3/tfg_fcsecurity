#!/usr/bin/python
import collections
import json
import optparse
import os

from backend.ai.features_extraction import FeaturesExtraction


def _parse_country_code(countrycode):
    countries = ["VA", "CC", "GT", "JP", "SE", "TZ", "CD", "GU", "MM", "DZ", "MN", "PK", "SG", "VC", "CF", "GW",
                 "MO", "PL", "SH", "CG", "MP", "PM", "SI", "VE", "ZW", "CH", "GY",
                 "MQ", "PN", "SJ", "CI", "MR", "SK", "VG", "MS", "SL", "CK", "ID", "MT", "SM", "VI", "YE", "CL",
                 "IE", "LA", "MU", "SN", "CM", "FI", "LB", "MV", "PR", "SO", "CN",
                 "FJ", "LC", "MW", "PS", "CO", "FK", "MX", "PT", "MY", "SR", "VN", "FM", "MZ", "CR", "PW", "CS",
                 "FO", "ST", "IL", "LI", "PY", "BA", "CU", "IM", "SV", "CV", "FR",
                 "IN", "LK", "BB", "IO", "VU", "CX", "RE", "UA", "SY", "CY", "IQ", "SZ", "BD", "CZ", "IR", "YT",
                 "BE", "IS", "BF", "EC", "IT", "OM", "BG", "BH", "LR", "UG", "BI",
                 "EE", "LS", "BJ", "LT", "EG", "EH", "LU", "RO", "BM", "LV", "BN", "UM", "BO", "KE", "NA", "LY",
                 "BR", "KG", "NC", "BS", "HK", "KH", "BT", "KI", "NE", "QA", "RU",
                 "HM", "NF", "US", "BV", "ER", "HN", "NG", "RW", "BW", "ES", "ET", "NI", "AD", "BY", "KM", "AE",
                 "BZ", "HR", "KN", "TC", "AF", "NL", "TD", "AG", "HT", "KP", "UY",
                 "GA", "HU", "TF", "UZ", "AI", "DE", "GB", "KR", "TG", "NO", "TH", "GD", "NP", "ZA", "AL", "GE",
                 "TJ", "WF", "AM", "GF", "NR", "TK", "AN", "DJ", "KW", "TL", "AO",
                 "DK", "GG", "TM", "GH", "JE", "MA", "KY", "NU", "TN", "DM", "GI", "KZ", "TO", "AQ", "MC", "AR",
                 "MD", "AS", "DO", "PA", "TR", "AT", "GL", "NZ", "AU", "GM", "MG",
                 "GN", "MH", "TT", "ZM", "AW", "PE", "SA", "AX", "GP", "JM", "PF", "SB", "TV", "WS", "CA", "GQ",
                 "SC", "TW", "AZ", "GR", "MK", "PG", "GS", "SD", "JO", "ML", "PH"]

    europe = ["AX", "AL", "AD", "AT", "BY", "BE", "BA", "BG", "HR", "CZ", "DK", "EE", "FO", "FI", "FR", "DE", "GI",
              "GR", "GG", "VA", "HU", "IS",
              "IE", "IM", "IT", "JE", "LV", "LI", "LT", "LU", "MK", "MT", "MD", "MC", "ME", "NL", "NO", "PL", "PT",
              "RO", "RU", "SM", "RS", "SK",
              "SI", "ES", "SJ", "SE", "CH", "UA", "GB"]

    nort_america = ["AI", "AG", "AW", "BS", "BB", "BZ", "BM", "VG", "CA", "KY", "CR", "CU", "DM", "DO", "SV", "GL",
                    "GD", "GP", "GT", "HT", "HN", "JM",
                    "MQ", "MX", "MS", "AN", "NI", "PA", "PR", "BL", "KN", "LC", "MF", "PM", "VC", "TT", "TC", "US",
                    "VI"]

    south_america = ["AR", "BO", "BR", "CL", "CO", "EC", "FK", "GF", "GY", "PY", "PE", "SR", "UY", "VE"]

    asia = ["AF", "AM", "AZ", "BH", "BD", "BT", "IO", "BN", "KH", "CN", "CX", "CC", "CY", "GE", "HK", "IN", "ID",
            "IR", "IQ", "IL", "JP", "JO",
            "KZ", "KP", "KR", "KW", "KG", "LA", "LB", "MO", "MY", "MV", "MN", "MM", "NP", "OM", "PK", "PS", "PH",
            "QA", "SA", "SG", "LK", "SY",
            "TW", "TJ", "TH", "TL", "TR", "TM", "AE", "UZ", "VN", "YE"]

    africa = ["DZ", "AO", "BJ", "BW", "BF", "BI", "CM", "CV", "CF", "TD", "KM", "CD", "CG", "CI", "DJ", "EG", "GQ",
              "ER", "ET", "GA", "GM",
              "GH", "GN", "GW", "KE", "LS", "LR", "LY", "MG", "MW", "ML", "MR", "MU", "YT", "MA", "MZ", "NA", "NE",
              "NG", "RE", "RW", "SH",
              "ST", "SN", "SC", "SL", "SO", "ZA", "SD", "SZ", "TZ", "TG", "TN", "UG", "EH", "ZM", "ZW"]

    oceania = ["AS", "AU", "CK", "FJ", "PF", "GU", "KI", "MH", "FM", "NR", "NC", "NZ", "NU", "NF", "MP", "PW", "PG",
               "PN", "WS", "SB", "TK", "TO",
               "TV", "UM", "VU", "WF"]

    country_code_dict = {}
    for cc in countries:
        country_code_dict["country_name_" + cc.lower()] = cc == countrycode

    country_code_dict["continent_name_europe"] = countrycode in europe
    country_code_dict["continent_name_nort_america"] = countrycode in nort_america
    country_code_dict["continent_name_south_america"] = countrycode in south_america
    country_code_dict["continent_name_asia"] = countrycode in asia
    country_code_dict["continent_name_africa"] = countrycode in africa
    country_code_dict["continent_name_oceania"] = countrycode in oceania

    return country_code_dict


def _parse_important_permissions(permissions):
    important_permissions = ["android.permission.CHANGE_WIFI_STATE",
                             "android.permission.CHANGE_NETWORK_STATE",
                             "android.permission.INSTALL_PACKAGES",
                             "com.android.launcher.permission.INSTALL_SHORTCUT",
                             "android.permission.SYSTEM_ALERT_WINDOW",
                             "android.permission.ACCESS_DOWNLOAD_MANAGER",
                             "android.permission.ACCESS_DOWNLOAD_MANAGER_ADVANCED",
                             "android.permission.MOUNT_FORMAT_FILESYSTEMS",
                             "android.permission.MOUNT_UNMOUNT_FILESYSTEMS",
                             "android.permission.INTERNET",
                             "android.permission.RECEIVE_BOOT_COMPLETED",
                             "android.permission.KILL_BACKGROUND_PROCESSES",
                             "android.permission.ACCESS_MTK_MMHW",
                             "android.permission.DISABLE_KEYGUARD",
                             "android.permission.SYSTEM_ALERT_WINDOW",
                             "android.permission.GET_TASKS",
                             "android.permission.INJECT_EVENTS",
                             "android.permission.INSTALL_GRANT_RUNTIME_PERMISSIONS",
                             "android.permission.KILL_UID",
                             "android.permission.BRICK",
                             "android.permission.READ_CALENDAR",
                             "android.permission.WRITE_CALENDAR",
                             "android.permission.CAMERA",
                             "android.permission.READ_CONTACTS",
                             "android.permission.WRITE_CONTACTS",
                             "android.permission.GET_ACCOUNTS",
                             "android.permission.ACCESS_FINE_LOCATION",
                             "android.permission.ACCESS_COARSE_LOCATION",
                             "android.permission.RECORD_AUDIO",
                             "android.permission.READ_PHONE_STATE",
                             "android.permission.CALL_PHONE",
                             "android.permission.READ_CALL_LOG",
                             "android.permission.WRITE_CALL_LOG",
                             "android.permission.ANSWER_PHONE_CALLS",
                             "com.android.voicemail.permission.ADD_VOICEMAIL",
                             "android.permission.USE_SIP",
                             "android.permission.PROCESS_OUTGOING_CALLS",
                             "android.permission.BODY_SENSORS",
                             "android.permission.SEND_SMS",
                             "android.permission.RECEIVE_SMS",
                             "android.permission.READ_SMS",
                             "android.permission.RECEIVE_WAP_PUSH",
                             "android.permission.RECEIVE_MMS",
                             "android.permission.READ_EXTERNAL_STORAGE",
                             "android.permission.WRITE_EXTERNAL_STORAGE"]

    return {perm.split('.')[-1].lower(): (perm in permissions) for perm in important_permissions}


def _parser_content_rating(content_rating):
    content_ratings = ["All ages", "Everyone", "PEGI 3", "USK: All ages", "Rated for 3+"]
    return {"cr_" + cr.replace(" ", "_").lower(): cr == content_rating for cr in content_ratings}


def _parser_category(category):
    categories_app = ["Art & Design", "Auto & Vehicles", "Beauty", "Books & Reference", "Business",
                      "Comics", "Communication", "Dating", "Education", "Enternainment", "Events",
                      "Finance", "Food & Drink", "Health & Fitness", "House & Demo", "Libraries & Demo",
                      "Lifestyle", "Maps & Navigation", "Medical", "Music & Audio", "News & Magazines",
                      "Parenting", "Personalization", "Photography", "Productivity", "Shopping", "Social",
                      "Sport", "Tools", "Travel & Local", "Video Players & Editors", "Weather"]

    categories_games = ["Action", "Adventure", "Arcade", "Board", "Card", "Casino", "Casual", "Educational",
                        "Music", "Puzzle", "Racing", "Role Playing", "Simulation", "Sports", "Strategy", "Trivia",
                        "Word"]

    category_dict = {}

    for ca in categories_app:
        category_dict["category_" + ca.replace(" ", "_").lower()] = ca == category
    for cg in categories_games:
        category_dict["category_" + cg.replace(" ", "_").lower()] = cg == category

    category_dict["type_category_apps"] = category in categories_app
    category_dict["type_category_games"] = category in categories_games

    return category_dict


def _parser_downloads(downloads):
    app_download_ranges = ["1-5", "5-10", "10-50", "50-100", "100-500", "500-1000", "1000-5000",
                           "5000-10000", "10000-50000", "50000-100000", "100000-500000", "500000-1000000",
                           "1000000-5000000", "5000000-10000000", "10000000-50000000", "50000000-100000000",
                           "100000000-500000000", "500000000-1000000000", "1000000000-5000000000"]
    return {"downloads_" + app_d_range: app_d_range == downloads for app_d_range in app_download_ranges}


'''
def _parse_range(prefix, num_range):
    return {prefix + str(i): i == num_range for i in range(10)}
'''


def _create_dir_if_needed(save_path):
    if not os.path.exists(os.path.dirname(save_path)):
        os.makedirs(os.path.dirname(save_path))


def _get_data_from_json(data):
    try:
        with open(data) as data_file:
            return json.load(data_file)
    except FileNotFoundError:
        return None


def load_alexa_top():
    import csv
    alexa_top = set()
    with open('backend/ai/top-1m.csv') as File:
        reader = csv.reader(File)
        for row in reader:
            alexa_top.add(row[1])
    return alexa_top


class PrepareReport():
    def __init__(self, data, training=False, is_raw_data=False):
        self.has_features_report = False
        self.data = data
        self.is_raw_data = is_raw_data
        self.training = training
        self.report = {}
        self.alexaTop = None

    def get_report(self):
        if self.is_raw_data:
            return self._prepare_report_from_raw_data()
        else:
            return self._prepare_report_from_features_report()

    def set_alexa_top(self, alexaTop):
        self.alexaTop = alexaTop

    def _prepare_report_from_raw_data(self):
        if bool(self.report): return self.report
        fExtraction = FeaturesExtraction(self.data)
        self.features_report = fExtraction.get_report()
        self.has_features_report = self.features_report is not None
        return self._get_parser_report()

    def _prepare_report_from_features_report(self):
        if bool(self.report): return self.report
        if isinstance(self.data, str):  # path
            self.features_report = _get_data_from_json(self.data)
        elif isinstance(self.data, dict):  # report
            self.features_report = self.data
        else:
            self.features_report = None
        self.has_features_report = self.features_report is not None
        return self._get_parser_report()

    def _get_parser_report(self):
        if not self.has_features_report:
            return None  # throw Exception
        if bool(self.report):
            return self.report  # report is not empty

        self.report["general"] = self.parse_general_data()
        self.report["activities"] = self.parse_activities_data()
        self.report["receivers"] = self.parse_receivers_data()
        self.report["services"] = self.parse_services_data()
        self.report["providers"] = self.parse_providers_data()
        self.report["certificate"] = self.parse_certificate_data()
        self.report["gplaydata"] = self.parse_gplay_data()
        self.report["permissions"] = self.parse_permissions_data()
        self.report["files"] = self.parse_files_data()
        self.report["strings"] = self.parse_strings_data()
        self.report["methods"] = self.parse_methods_data()
        self.report["specials"] = self.parse_special_data()
        self.report["z"] = {'sha256': self.features_report["general"]["sha256"]}
        if self.training:
            self.report["z"]['is_malware'] = self.is_malware()
        return collections.OrderedDict(sorted(self.report.items()))

    def parse_general_data(self):
        if not self.has_features_report: return None
        features_general = self.features_report["general"]
        min_sdk = features_general["minimun_sdk_version"]
        version_code = int(features_general["version_code"]) if features_general["version_code"] != "" else 0
        return {"minimun_sdk_version": int(min_sdk) if min_sdk is not None else 0,
                "version_code": version_code,
                "file_size": features_general["file_size"]}

    def parse_activities_data(self):
        if not self.has_features_report:
            return None
        features_activities = self.features_report["activities"]
        return {"size": features_activities["size"]}

    def parse_receivers_data(self):
        if not self.has_features_report:
            return None
        features_receivers = self.features_report["receivers"]
        return {"size": features_receivers["size"]}

    def parse_services_data(self):
        if not self.has_features_report:
            return None
        features_services = self.features_report["services"]
        return {"size": features_services["size"]}

    def parse_providers_data(self):
        if not self.has_features_report:
            return None
        features_providers = self.features_report["providers"]
        return {"size": features_providers["size"]}

    def parse_certificate_data(self):
        if not self.has_features_report:
            return None
        features_certificate = self.features_report["certificate"]
        parse_certificate = {"has_android_certificate": features_certificate["has_android_certificate"],
                             "android_debug": features_certificate["android_debug"]}
        parse_certificate.update(_parse_country_code(features_certificate["country_name"]))
        return parse_certificate

    def parse_gplay_data(self):
        if not self.has_features_report:
            return None
        parse_gplaydata_report = {}
        features_gplaydata = self.features_report["gplaydata"]
        exists_in_googleplay = features_gplaydata["exists"]
        parse_gplaydata_report["exists"] = exists_in_googleplay
        if exists_in_googleplay:
            parse_gplaydata_report.update(_parser_category(features_gplaydata["category"]))
            parse_gplaydata_report.update(_parser_downloads(features_gplaydata["downloads"]))
            parse_gplaydata_report["score"] = float(features_gplaydata["score"])
            parse_gplaydata_report.update(_parser_content_rating(features_gplaydata["content_rating"]))
        else:
            parse_gplaydata_report.update(_parser_category("other"))
            parse_gplaydata_report.update(_parser_downloads("other"))
            parse_gplaydata_report.update(_parser_content_rating("other"))
            parse_gplaydata_report["score"] = 0
        return parse_gplaydata_report

    def parse_permissions_data(self):
        if not self.has_features_report:
            return None
        parse_permissions = {}
        features_permissions = self.features_report["permissions"]
        parse_permissions["total"] = features_permissions["total_size"]
        parse_permissions["declared"] = features_permissions["declared_size"]
        parse_permissions["aosp"] = features_permissions["aosp_size"]
        parse_permissions["third_party"] = features_permissions["third_party_size"]
        parse_permissions.update(_parse_important_permissions(features_permissions["names"]))
        return parse_permissions

    def parse_files_data(self):
        features_files = self.features_report["files"]
        files_real_total_number = features_files["total_size"]
        return {"total_size": files_real_total_number,
                "dex_size": features_files["dex_size"],
                "xml_size": features_files["xml_size"],
                "elf_size": features_files["elf_size"],
                "zip_size": features_files["zip_size"],
                "jar_size": features_files["jar_size"],
                "sf_size": features_files["sf_size"],
                "rsa_size": features_files["rsa_size"],
                "mf_size": features_files["mf_size"],
                "apk_size": features_files["apk_size"],
                "dll_size": features_files["dll_size"]}

    def parse_strings_data(self):
        features_strings = self.features_report["strings"]

        if not self.alexaTop:
            print('load alexa')
            self.alexaTop = load_alexa_top()

        out_alexa_top = [host for host in features_strings["single_hosts"] if host not in self.alexaTop] + \
                        features_strings["single_ips"]

        return {"unique_urls": features_strings["unique_urls"],
                "single_hosts_size": features_strings["single_hosts_size"],
                "single_domains_size": features_strings["single_domains_size"],
                "single_ips_size": features_strings["single_ips_size"],
                "http_schemes_size": features_strings["http_schemes_size"],
                "https_schemes_size": features_strings["https_schemes_size"],
                "out_alexa_top_size": len(out_alexa_top),
                "dex_size": features_strings["dex_size"],
                "xml_size": features_strings["xml_size"],
                "elf_size": features_strings["elf_size"],
                "zip_size": features_strings["zip_size"],
                "jar_size": features_strings["jar_size"],
                "sf_size": features_strings["sf_size"],
                "rsa_size": features_strings["rsa_size"],
                "mf_size": features_strings["mf_size"],
                "apk_size": features_strings["apk_size"],
                "dll_size": features_strings["dll_size"]}

    def parse_methods_data(self):
        features_methods = self.features_report["methods"]
        return {"total_calls": features_methods["total_calls"],
                "unique_methods_size": features_methods["unique_methods_size"]}

    def parse_special_data(self):
        return {"good_developer": self.features_report["specials"]["good_developer"]}

    def is_malware(self):
        from backend.ai.vt import VtScan
        label = VtScan().scan(self.features_report["general"]["sha256"])
        return float(label == VtScan.POSITIVE)
        '''
        {"positive": float(label == VtScan.POSITIVE),
        "negative": float(label == VtScan.NEGATIVE),
        "unknown": float(label == VtScan.UNKNOWN)}
        '''

    '''
    def get_array_report(self):
        if not self.has_features_report: return None
        report = self._get_parser_report()
        headers = []
        array = []
        for big_header in report:
            current_report = report[big_header]
            for header, value in current_report.items():
                headers.append(big_header + "_" + header)
                array.append(float(value))

        if self.training:
            headers.append("z_malware")
            array.append(self.report["z"]["malware"]["positive"] == 1.0)

        headers.append("z_sha256")
        array.append(self.report["z"]["sha256"])
        
        return headers, array

    def save_array_report(self, save_path):
        if not self.has_features_report:
            return None
        headers, array = self.get_array_report()
        _create_dir_if_needed(save_path)
        with open(save_path, 'w') as outfile:
            outfile.write(str(headers)[1:-1] + "\n")  # headers
            outfile.write(str(array)[1:-1])  # data
    '''

    def save_report(self, save_path):
        if not self.has_features_report:
            return None

        _create_dir_if_needed(save_path)
        with open(save_path, 'w') as outfile:
            outfile.write(json.dumps(self._get_parser_report(), sort_keys=True, indent=3))


if __name__ == "__main__":
    """Prepare ML Report from ApkFile"""
    usage = 'usage: %prog [options] <apkFile> <dest_report>'
    parser = optparse.OptionParser(
        usage=usage,
        description='Prepare a ml report from apk'
                    'Prepare report. Examples:'
                    'python %prog ./features_report.json ./malicious_report.json'
                    'python %prog -t -r ./malicious.apk ./malicious_report.json'
                    'python %prog -t -r ./raw_report.json ./malicious_report.json'
        # 'python %prog -t -r -a ./malicious_array.txt ./malicious.apk ./malicious_report.json'
    )

    parser.add_option(
        '-r', '--rawdata', action='store_true', dest='is_raw_data',
        help='is file to scan or raw report'
    )

    parser.add_option(
        '-t', '--training', action='store_true', dest='training',
        help='get label from VirusTotal in order to train')
    '''
    parser.add_option(
        '-a', '--array', dest='arrayPath', default='',
        help='save array report in this path')
    '''
    (options, args) = parser.parse_args()
    if not args:
        parser.error('No apk file provided')

    is_raw_data = options.is_raw_data
    training = options.training
    # arrayPath = options.arrayPath

    apkFile = args[0]
    if len(args) > 1:
        destination = args[1]
    else:
        reportName = apkFile.split('/')[-1].split('.')[-2]
        reportDir = os.path.dirname(os.path.abspath(__file__)) + "/samples/prepare-reports/"
        destination = reportDir + reportName + '.json'

    pReport = PrepareReport(apkFile, training=training, is_raw_data=is_raw_data)
    report = pReport.get_report()

    if report is None:
        print("File {} not exist".format(apkFile))
    else:
        pReport.save_report(destination)
        '''
        if bool(arrayPath):
            pReport.save_array_report(arrayPath)
        '''
