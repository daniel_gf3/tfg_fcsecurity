#!/usr/bin/python
import json
import optparse
import os

from cryptography.x509.oid import NameOID

from backend.ai.data_extraction.androlyze import AnalyzeAPK


def get_clean_receivers_report():
    receivers = get_clean_receivers_report()
    if receivers is None:
        return None
    receivers["size"] = len(receivers["names"])
    return receivers


def _get_special_report(report):
    if "specials" in report:
        return report["specials"]
    # gdg developer name is the certificate name
    special = {}
    gplay_exists = report["gplaydata"]["exists"]
    if gplay_exists:
        gplay_developer_name = report["gplaydata"]["developer_name"]
        certificate_organization_name = report["certificate"]["organization_name"]
        special["good_developer"] = gplay_exists and (gplay_developer_name == certificate_organization_name)
    else:
        special["good_developer"] = False
    return special


def _get_attribute_for_oid(certificate, nameOID):
    try:
        subject = certificate.subject
        attributesOID = subject.get_attributes_for_oid(nameOID)
        return attributesOID[0].value if len(attributesOID) >= 0 else ""
    except:
        return ""


def _google_play_data(package_name):
    from bs4 import BeautifulSoup
    import requests
    gplay_report = {}

    url = "https://play.google.com/store/apps/details?hl=en&id=" + package_name
    req = requests.get(url)
    status_code = req.status_code
    gplay_report["exists"] = False

    if status_code == 200:
        html = BeautifulSoup(req.text, "html.parser")
        title = html.find('title')
        if title != "Not Found":
            gplay_report["exists"] = True
            category = html.find('a', {'itemprop': 'genre'})
            num_donwloads = html.findAll('span', {'class': 'htlgb'})[4]
            name = html.find('h1', {'itemprop': 'name'})
            developer_name = html.find('a',{'class':'hrTbp R8zArc'})

            score = html.find('div', {'class': 'BHMmbe'})
            rating_count = html.find('span', {'class': 'EymY4b'})
            content_rating = html.find('img', {'class': 'T75of pTsWIc'})

            gplay_report["category"] = category.getText() if category is not None else ""
            gplay_report["downloads"] = ' '.join(num_donwloads.getText()).replace(" ", "").replace(",",
                                                                                                "") if num_donwloads is not None else '0'
            gplay_report["name"] = ' '.join(name.getText()).replace(" ", "") if name is not None else ""
            gplay_report["developer_name"] = developer_name.getText() if developer_name is not None else ""
            gplay_report["score"] = score.getText() if score is not None else '0'
            gplay_report["rating_count"] = rating_count.getText().split(' ')[0] if rating_count is not None else '0'
            gplay_report["content_rating"] = content_rating['alt'] if content_rating is not None else '0'

    return gplay_report


def _files_data(files):
    return {"names": [str(f) for f in files]}


def _files_data_full(a):
    files = a.get_files_types()
    return {"names": [{k: files[k]} for k in files]}


def _strings_data(d):
    strings = []
    if type(d) == list:
        for itemD in d:
            strings += itemD.get_strings()
    elif d is not None:
        strings = d.get_strings()
    return strings


def _methods_data(d):
    methods = []
    if type(d) == list:
        for itemD in d:
            methods += [m.name for m in itemD.get_methods()]
    elif d is not None:
        methods = [m.name for m in d.get_methods()]
    return methods


def _sha256_checksum(filename, block_size=65536):
    import hashlib
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            sha256.update(block)
    return sha256.hexdigest()


def _create_dir_if_needed(save_path):
    if not os.path.exists(os.path.dirname(save_path)):
        os.makedirs(os.path.dirname(save_path))


def _certificate_data(certificate):
    organization_name = _get_attribute_for_oid(certificate, NameOID.ORGANIZATION_NAME)
    common_name = _get_attribute_for_oid(certificate, NameOID.COMMON_NAME)
    certificate_report = {"has_android_certificate": certificate is not None,
                          "country_name": _get_attribute_for_oid(certificate, NameOID.COUNTRY_NAME),
                          "state_or_province_name": _get_attribute_for_oid(certificate,
                                                                           NameOID.STATE_OR_PROVINCE_NAME),
                          "locality_name": _get_attribute_for_oid(certificate, NameOID.LOCALITY_NAME),
                          "organization_name": organization_name,
                          "organization_unit_name": _get_attribute_for_oid(certificate,
                                                                           NameOID.ORGANIZATIONAL_UNIT_NAME),
                          "common_name": common_name,
                          "android_debug": common_name == "Android Debug" and organization_name == "Android"}

    if certificate is not None:
        from cryptography.hazmat.primitives import hashes
        import binascii
#        sha1 = str(binascii.hexlify(certificate.fingerprint(hashes.SHA1()))).decode("ascii")
        sha1 = str(binascii.hexlify(certificate.fingerprint(hashes.SHA1())).decode("ascii")) # Update 3/oct/2018
    else:
        sha1 = ""
    certificate_report["certificate_sha1"] = sha1

    return certificate_report


class DataExtraction:
    def __init__(self, apkpath):
        self.apkpath = apkpath
        self.has_analysis = False
        self.apk_report = {}
        self._analyze_apk()

    def _analyze_apk(self):
        try:
            self.a, self.d, self.dx = AnalyzeAPK(self.apkpath)
            self.has_analysis = True
        except:
            return None

    def get_report(self):
        if not self.has_analysis:
            return None

        if bool(self.apk_report):
            return self.apk_report  # apk_report is not empty
        try:
            # make report
            self.apk_report["general"] = self.get_general_report()  # Added file size
            self.apk_report["activities"] = self.get_activities_report()
            self.apk_report["receivers"] = self.get_receivers_report()
            self.apk_report["services"] = self.get_services_report()
            self.apk_report["providers"] = self.get_providers_report()
            self.apk_report["permissions"] = self.get_permissions_report()
            self.apk_report["certificate"] = self.get_certificate_report()
            self.apk_report["gplaydata"] = self.get_gplay_report()
            self.apk_report["files"] = self.get_files_report()
            self.apk_report["strings"] = self.get_strings_report()  # Updated
            # NEWS
            self.apk_report["methods"] = self.get_methods_report()  # New
            # END NEWS
            self.apk_report["specials"] = _get_special_report(self.apk_report)
            return self.apk_report
        except Exception as e:
            print('Error:', e)
            self.has_analysis = False
            return None

    def get_general_report(self):
        if not self.has_analysis:
            return None
        if "general" in self.apk_report:
            return self.apk_report["general"]
        general = {"file_size": os.path.getsize(self.apkpath)}
        try:
#            general["apk_name"] = self.a.get_app_name().decode("utf-8")
            general["apk_name"] = self.a.get_app_name()
        except:
            general["apk_name"] = ""

        general["package_name"] = self.a.get_package()
        general["minimun_sdk_version"] = self.a.get_min_sdk_version()
        try:
            general["version_code"] = self.a.get_androidversion_code()
        except:
            general["version_code"] = -1
        general["sha256"] = _sha256_checksum(self.apkpath)
        return general

    def get_clean_general_report(self):
        return self.get_general_report()

    def get_activities_report(self):
        if not self.has_analysis:
            return None
        if "activities" in self.apk_report:
            return self.apk_report["activities"]
        activities = {"main": self.a.get_main_activity(),
                      "names": self._get_intent_filters(self.a.get_activities(), "activity")}
        return activities

    def get_clean_activities_report(self):
        activities = self.get_activities_report()
        if activities is None: return None
        activities["size"] = len(activities["names"])
        return activities

    def get_receivers_report(self):
        if not self.has_analysis:
            return None
        if "receivers" in self.apk_report:
            return self.apk_report["receivers"]
        return {"names": self._get_intent_filters(self.a.get_receivers(), "receiver")}

    def get_services_report(self):
        if not self.has_analysis:
            return None
        if "services" in self.apk_report:
            return self.apk_report["services"]
        return {"names": self._get_intent_filters(self.a.get_services(), "service")}

    def get_clean_services_report(self):
        services = self.get_services_report()
        if services is None:
            return None
        services["size"] = len(services["names"])
        return services

    def _get_intent_filters(self, items, category):
        return [{"name": i, "intent_filters": self.a.get_intent_filters(category, i)}
                for i in items]
#        return [{"name": i.decode("utf-8"), "intent_filters": self.a.get_intent_filters(category, i.decode("utf-8"))}
#                for i in items]

    def get_providers_report(self):
        if not self.has_analysis:
            return None
        if "providers" in self.apk_report:
            return self.apk_report["providers"]
        #return {"names": [s.decode("utf-8") for s in self.a.get_providers()]}
        return {"names": [str(s) for s in self.a.get_providers()]}

    def get_permissions_report(self):
        if not self.has_analysis:
            return None
        if "permissions" in self.apk_report:
            return self.apk_report["permissions"]

        permissions = {"names": self.a.get_permissions(),
                       "declared": self.a.get_declared_permissions(),
                       "aosp": self.a.get_requested_aosp_permissions(),
                       "third_party": self.a.get_requested_third_party_permissions()}

        return permissions

    def get_certificate_report(self):
        if not self.has_analysis:
            return None
        if "certificate" in self.apk_report:
            return self.apk_report["certificate"]
        certificate = {}
        has_android_certificate = "META-INF/CERT.RSA" in self.a.get_signature_names()
        cert = self.a.get_certificate("META-INF/CERT.RSA") if has_android_certificate else None
        certificate.update(_certificate_data(cert))
        return certificate

    def get_gplay_report(self):
        if not self.has_analysis:
            return None
        if "gplaydata" in self.apk_report:
            return self.apk_report["gplaydata"]
        return _google_play_data(self.a.get_package())

    def get_files_report(self):
        if not self.has_analysis:
            return None
        if "files" in self.apk_report:
            return self.apk_report["files"]
        '''
        try:
            return _files_data_full(self.a)
        except:
            return _files_data(self.a.get_files())
        '''
        return _files_data(self.a.get_files())

    def get_strings_report(self):
        if not self.has_analysis:
            return None
        if "strings" in self.apk_report:
            return self.apk_report["strings"]
        return _strings_data(self.d)

    def get_methods_report(self):
        if not self.has_analysis:
            return None
        if "methods" in self.apk_report:
            return self.apk_report["methods"]
        return _methods_data(self.d)

    def save_report(self, save_path):
        report = self.get_report()
        if not self.has_analysis:
            return False
        _create_dir_if_needed(save_path)
        with open(save_path, 'w') as outfile:
            outfile.write(json.dumps(report, sort_keys=True, indent=3))
        return True


if __name__ == "__main__":
    """Extract Data from ApkFile"""
    usage = 'usage: %prog [options] <apkFile>'
    parser = optparse.OptionParser(
        usage=usage,
        description='Analyze and extracts a report from apk'
                    'Data Extraction. Example:'
                    'python %prog ./malicious.apk ./malicious_data_report.json')

    (option, args) = parser.parse_args()
    if not args:
        parser.error('No apk file provided')

    apkFile = args[0]
    if len(args) > 1:
        destination = args[1]
    else:
        reportDir = os.path.dirname(os.path.abspath(__file__)) + "/samples/reports/"
        reportName = apkFile.split('/')[-1].replace(".apk", "") + ".json"
        destination = reportDir + reportName

    dExtraction = DataExtraction(apkFile)
    report = dExtraction.get_report()
    if report is None:
        print('Is not APK File or not exist!!')
    else:
        dExtraction.save_report(destination)
