#!/usr/bin/python
import logging
import optparse
import os
import sys
import time
from os.path import isfile, join
from backend.ai.data_extraction.data_extraction import DataExtraction


LOGGING_LEVEL = logging.INFO  # Modify if you just want to focus on errors
logging.basicConfig(level=LOGGING_LEVEL,
                    format='%(asctime)s %(threadName)-10s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=sys.stdout)


def main():
    """Extract Data from ApkFile"""
    usage = 'usage: %prog [options] <apkFile>'
    parser = optparse.OptionParser(
        usage=usage,
        description='Analyze and extracts a report from apk'
                    'Data Extraction. Example:'
                    'python %prog ./malicious.apk ./malicious_data_report.json'
                    'python %prog -r ./malicious.apk ./malicious_data_report.json')

    parser.add_option(
        '-r', '--removesources', action='store_true', dest='remove_files', default=False,
        help='remove file after analyze'
    )

    (options, args) = parser.parse_args()
    if not args:
        parser.error('No directory provided')

    remove_files = options.remove_files

    source_dir = args[0]
    if len(args) > 1:
        destination_dir = args[1]
    else:
        destination_dir = os.path.dirname(os.path.abspath(__file__)) + "/samples/raw_reports/"

    def job(source_directory, destination_directory, remove_files):
        logging.info('Start Job')

        def is_already_analyzed(file, apks_analyzed):
            return file in apks_analyzed  # tendremos que sacar el hash, ahora usamos el nombre

        def save_raw_report(file):
            logging.info("Getting report. File: %s", file)
            data = DataExtraction(source_directory + file)
            logging.info("Saving report")
            success = data.save_report(destination_directory + file + ".json")
            if success:
                logging.info("Saved report")
            else:
                logging.info("Don't have report")
            return success

        def delete_file(file):
            os.remove(source_directory + file)

        def move_file(file, destination):
            create_dir_if_needed(destination)
            os.rename(source_directory + file, destination + file)

        def get_apks_files():
            return [f for f in os.listdir(source_directory) if
                    isfile(join(source_directory, f)) and not f.startswith('.')]

        def read_old_analyzed_apks():
            try:
                path = destination_directory + 'apks_analyzed.txt'
                with open(path, 'rb') as f:
                    old_list = f.readlines()

                old_list = [x.strip() for x in old_list]
                return old_list
            except:
                return []

        def save_analyzed_apk(apk):
            path = destination_directory + 'apks_analyzed.txt'
            create_dir_if_needed(path)
            with open(path, 'a+') as f:
                f.write("{}\n".format(apk))

        def save_conflicted_apk(apk):
            path = destination_directory + 'conflicted_apks_analyzed.txt'
            create_dir_if_needed(path)
            with open(path, 'a+') as f:
                f.write("{}\n".format(apk))

        def create_dir_if_needed(save_path):
            if not os.path.exists(os.path.dirname(save_path)):
                os.makedirs(os.path.dirname(save_path))

        BATCH = 30
        count = 0

        apks_analyzed = read_old_analyzed_apks()
        end_process = False
        waiting = False
        try:
            while not end_process:

                if count == BATCH:
                    time.sleep(60 * 6)  # wait a minute
                    # count = 0
                    quit()

                apks = get_apks_files()
                if len(apks) > 0:
                    waiting = False
                    current_apk = apks[0]
                    if is_already_analyzed(current_apk, apks_analyzed):
                        logging.info("Already existed the report for the file %s", current_apk)
                    else:
                        success = save_raw_report(current_apk)
                        apks_analyzed.append(
                            current_apk)  #  ahora guardamos el nombre, en el futuro guardaremos el hash
                        save_analyzed_apk(current_apk)
                        if not success:
                            save_conflicted_apk(current_apk)

                    if remove_files:
                        logging.info("Deleting file %s", current_apk)
                        delete_file(current_apk)
                    else:
                        logging.info("Moving file %s to %s", current_apk, (source_directory + "scaned_files/"))
                        move_file(current_apk, source_directory + "scaned_files/")
                else:
                    if not waiting:
                        logging.info('-- Waiting for samples --')
                    waiting = True
                    time.sleep(60)  # wait a minute
                count += 1
        except KeyboardInterrupt:
            end_process = True
            logging.info('------- Stopping the process -------')
        '''
        logging.info('saving analyzed_apks')
        save_analyzed_apks(apks_analyzed)
        '''
        logging.info('------- END PROGRAM -------')
        quit()

    #  Start job
    job(source_dir, destination_dir, remove_files)


if __name__ == "__main__":
    main()

# /Volumes/BIG_STORE/Documents/tfg/intelligencefiles/20171014T101817/
