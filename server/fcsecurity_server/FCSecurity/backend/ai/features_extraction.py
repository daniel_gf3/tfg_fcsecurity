#!/usr/bin/python
from backend.ai.data_extraction.data_extraction import DataExtraction
from backend.ai.urlextract import URLExtract, get_host_from_url_without_www, get_scheme_from_url, get_domain_from_url
import json
import optparse
import os


def _get_data_from_json(data):
    try:
        with open(data) as data_file:
            return json.load(data_file)
    except FileNotFoundError:
        return None


def _create_dir_if_needed(save_path):
    if not os.path.exists(os.path.dirname(save_path)):
        os.makedirs(os.path.dirname(save_path))


class FeaturesExtraction:
    def __init__(self, data):
        if isinstance(data, str):  # path
            split_data = data.split('.')
            if split_data[-1] == "json":
                self.rawReport = _get_data_from_json(data)
            else:
                self.rawReport = DataExtraction(data).get_report()
        elif isinstance(data, dict):  # report
            self.rawReport = data
        else:
            self.rawReport = None
        self.report = {}
        self.has_raw_data = self.rawReport is not None

    def get_report(self):
        if not self.has_raw_data:
            return None
        if bool(self.report):
            return self.report  # report is not empty

        self.report["general"] = self.get_general_report()
        self.report["activities"] = self.get_activities_report()
        self.report["receivers"] = self.get_receivers_report()
        self.report["services"] = self.get_services_report()
        self.report["providers"] = self.get_providers_report()
        self.report["certificate"] = self.get_certificate_report()
        self.report["gplaydata"] = self.get_gplaydata_report()
        self.report["permissions"] = self.get_permissions_report()
        self.report["files"] = self.get_files_report()
        self.report["strings"] = self.get_strings_report()
        self.report["methods"] = self.get_methods_report()
        self.report["specials"] = self.get_specials_report()
        return self.report

    def get_general_report(self):
        if not self.has_raw_data:
            return None
        
        version_code = self.rawReport["general"]["version_code"]
        version_code = int(version_code) if version_code != "" else 0
        self.rawReport["general"]["version_code"] = version_code
        return self.rawReport["general"]

    # TODO metemos algo de intent-filters
    def get_activities_report(self):
        if not self.has_raw_data:
            return None
        raw_activities = self.rawReport["activities"]
        return {"names": raw_activities["names"], "size": len(raw_activities["names"])}

    # TODO metemos algo de intent-filters
    def get_receivers_report(self):
        if not self.has_raw_data:
            return None
        raw_receivers = self.rawReport["receivers"]
        return {"names": raw_receivers["names"], "size": len(raw_receivers["names"])}

    # TODO metemos algo de intent-filters
    def get_services_report(self):
        if not self.has_raw_data:
            return None
        raw_services = self.rawReport["services"]
        return {"names": raw_services["names"], "size": len(raw_services["names"])}

    # TODO metemos algo de intent-filters
    def get_providers_report(self):
        if not self.has_raw_data:
            return None
        raw_providers = self.rawReport["providers"]
        return {"names": raw_providers["names"], "size": len(raw_providers["names"])}

    def get_certificate_report(self):
        if not self.has_raw_data:
            return None
        raw_certificate = self.rawReport["certificate"]
        return {key: value for key, value in raw_certificate.items()}

    def get_gplaydata_report(self):
        if not self.has_raw_data:
            return None
        raw_gplaydata = self.rawReport["gplaydata"]
        return {key: value for key, value in raw_gplaydata.items()}

    def get_permissions_report(self):
        if not self.has_raw_data:
            return None
        raw_permissions = self.rawReport["permissions"]
        return {"names": raw_permissions["names"], "total_size": len(raw_permissions["names"]),
                "declared": raw_permissions["declared"], "declared_size": len(raw_permissions["declared"]),
                "aosp": raw_permissions["aosp"], "aosp_size": len(raw_permissions["aosp"]),
                "third_party": raw_permissions["third_party"],
                "third_party_size": len(raw_permissions["third_party"])}

    def get_files_report(self):
        if not self.has_raw_data:
            return None
        raw_files_names = self.rawReport["files"]["names"]

        names = []
        # revisar este bucle
        '''
        for item in raw_files_names:
            for name in item.keys():
                names.append(name)
        '''

        files = {"total_size": len(raw_files_names),
                 "dex": [],
                 "xml": [],
                 "elf": [],
                 "zip": [],
                 "jar": [],
                 "sf": [],
                 "rsa": [],
                 "mf": [],
                 "apk": [],
                 "dll": []}

        for item in raw_files_names:
            oldReport = isinstance(item, str)
            if oldReport:
                key = item
                value = ""
            else:
                key = list(item.keys())[0]
                value = item[key]

            if (not value and key.endswith('.dex')) or  value.startswith('Dalvik dex file'):
                files["dex"].append(key)
            elif (not value and key.endswith('.xml')) or value == 'Android binary XML':
                files["xml"].append(key)
            elif (not value and (key.endswith(".axf") or key.endswith(".bin") or key.endswith(".elf") or key.endswith(".o") or key.endswith(".prx") or key.endswith(".ko") or key.endswith(".mod") or key.endswith(".so"))) or value.startswith('ELF '):
                files["elf"].append(key)
            elif key.endswith('.apk'):
                files["apk"].append(key)
            elif (not value and key.endswith('.zip')) or value.startswith('Zip archive data'):
                files["zip"].append(key)
            elif (not value and key.endswith('.jar')) or value == 'Java archive data (JAR)':
                files["jar"].append(key)
            elif key.endswith('.SF'):
                files['sf'].append(key)
            elif key.endswith('.RSA'):
                files['rsa'].append(key)
            elif key.endswith('.MF'):
                files["mf"].append(key)
            elif key.endswith('.dll'):
                files["dll"].append(key)

        files["dex_size"] = len(files["dex"])
        files["xml_size"] = len(files["xml"])
        files["elf_size"] = len(files["elf"])
        files["zip_size"] = len(files["zip"])
        files["jar_size"] = len(files["jar"])
        files["sf_size"] = len(files["sf"])
        files["rsa_size"] = len(files["rsa"])
        files["mf_size"] = len(files["mf"])
        files["apk_size"] = len(files["apk"])
        files["dll_size"] = len(files["dll"])
        return files

    def get_strings_report(self):
        if not self.has_raw_data:
            return None
        raw_strings_names = self.rawReport["strings"]
        strings = {
            "urls": [],
            "dex": [],
            "xml": [],
            "elf": [],
            "zip": [],
            "jar": [],
            "sf": [],
            "rsa": [],
            "mf": [],
            "apk": [],
            "dll": []
        }
        # URLS
        extract = URLExtract()
        for line in raw_strings_names:
            strings["urls"] += extract.find_urls(line)

            # Files
            if ".dex " in line:
                strings["dex"].append(line)
            elif ".xml " in line:
                strings["xml"].append(line)
            elif ".axf " in line or ".bin " in line or ".elf " in line or ".o " in line or ".prx " in line or ".ko " in line or ".mod " in line or ".so " in line:
                strings["elf"].append(line)
            elif ".zip " in line:
                strings["zip"].append(line)
            elif ".jar " in line:
                strings["jar"].append(line)
            elif ".SF " in line:
                strings["sf"].append(line)
            elif ".RSA " in line:
                strings["rsa"].append(line)
            elif ".MF " in line:
                strings["mf"].append(line)
            elif ".apk " in line:
                strings["apk"].append(line)
            elif ".dll " in line:
                strings["dll"].append(line)

        # urls
        strings["urls"] = list(set(strings["urls"]))
        strings["unique_urls"] = len(strings["urls"])

        # hosts
        single_hosts = list(set([get_host_from_url_without_www(url) for url in strings["urls"]]))
        strings["single_hosts"] = single_hosts
        strings["single_hosts_size"] = len(single_hosts)

        # domains
        single_domains = list(set([get_domain_from_url(url) for url in strings["urls"]]))
        strings["single_domains"] = single_domains
        strings["single_domains_size"] = len(single_domains)

        # ips
        strings["single_ips"] = list(set([host for host in single_hosts if extract.is_ip_the_host(host)]))
        strings["single_ips_size"] = len(strings["single_ips"])
        schemes = {url: get_scheme_from_url(url) for url in strings["urls"]}
        strings["http_schemes"] = list(set([url for url, scheme in schemes.items() if scheme == "http"]))
        strings["http_schemes_size"] = len(strings["http_schemes"])
        strings["https_schemes"] = list(set([url for url, scheme in schemes.items() if scheme == "https"]))
        strings["https_schemes_size"] = len(strings["https_schemes"])

        # FILES
        strings["dex_size"] = len(strings["dex"])
        strings["xml_size"] = len(strings["xml"])
        strings["elf_size"] = len(strings["elf"])
        strings["zip_size"] = len(strings["zip"])
        strings["jar_size"] = len(strings["jar"])
        strings["sf_size"] = len(strings["sf"])
        strings["rsa_size"] = len(strings["rsa"])
        strings["mf_size"] = len(strings["mf"])
        strings["apk_size"] = len(strings["apk"])
        strings["dll_size"] = len(strings["dll"])
        return strings

    def get_methods_report(self):
        if not self.has_raw_data:
            return None
        raw_methods_names = self.rawReport["methods"]
        clean_methods = [m for m in raw_methods_names if '<' not in m and '>' not in m]
        total_calls = len(clean_methods)
        unique_methods = list(set(clean_methods))
        return {"total_calls": total_calls,
                "unique_methods_size": len(unique_methods),
                "unique_methods": unique_methods}

    def get_specials_report(self):
        if not self.has_raw_data:
            return None
        raw_special = self.rawReport["specials"]
        return {"good_developer": raw_special["good_developer"]}

    def save_report(self, save_path):
        if not self.has_raw_data:
            return None
        _create_dir_if_needed(save_path)
        with open(save_path, 'w') as outfile:
            outfile.write(json.dumps(self.get_report(), sort_keys=True, indent=3))


if __name__ == "__main__":
    """Prepare Report from ApkFile"""
    usage = 'usage: %prog [options] <apkFile>'
    parser = optparse.OptionParser(
        usage=usage,
        description='Prepare a report from apk'
                    'Prepare report. Example:'
                    'python %prog ./malicious.apk ./malicious_report.json'
    )

    (option, args) = parser.parse_args()

    if not args:
        parser.error('No apk file provided')

    apkFile = args[0]

    if len(args) > 1:
        destination = args[1]
    else:
        reportDir = os.path.dirname(os.path.abspath(__file__)) + "/samples/parser-reports/"
        reportName = apkFile.split('/')[-1].split('.')[0] + ".json"
        destination = reportDir + reportName

    fExtraction = FeaturesExtraction(apkFile)
    report = fExtraction.get_report()

    if report is None:
        print("File {} not exist".format(apkFile))
    else:
        fExtraction.save_report(destination)
