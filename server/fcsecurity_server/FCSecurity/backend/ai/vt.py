#!/usr/bin/python
import requests


class VtScan:
    UNKNOWN = -1
    NEGATIVE = 0
    POSITIVE = 1

    def __init__(self):
        self.apikey = 'b5a83874d2b570f72949c64786a2336a2c9b37ac9235b52c2541c56bec66a60e'

    def scan(self, sha256):
        
        url = 'https://www.virustotal.com/vtapi/v2/file/report'
        params = {'apikey': self.apikey, 'resource': sha256}
        response = requests.get(url, params=params)
        if response.status_code == 200:
            json_response = response.json()
            if json_response is not None:
                if json_response["positives"] > 3:
                    return VtScan.POSITIVE
                else:
                    return VtScan.NEGATIVE

        return VtScan.UNKNOWN
