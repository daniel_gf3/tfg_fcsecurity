import hdbscan
import pandas as pd
from sklearn.model_selection import train_test_split

import backend.ai.ai_utils as ai_utils

DATASET_NAME = 'backend/ai/dataset.csv'

GOODWARE_CLUSTER_NAME = 'backend/ai/goodware_cluster.pkl'
MALWARE_CLUSTER_NAME = 'backend/ai/malware_cluster.pkl'

SCALER_NAME = 'backend/ai/scaler.pkl'


def predict_samples_v1(goodware_clusterer, malware_clusterer, samples):
    # malware:1, goodware: 0 unknown:-1
    labels_gw, strengths_gw = hdbscan.approximate_predict(goodware_clusterer, samples)
    labels_mw, strengths_mw = hdbscan.approximate_predict(malware_clusterer, samples)
    pred_gw = [0 if l != -1 else -1 for l in labels_gw]
    pred_mw = [1 if l != -1 else -1 for l in labels_mw]
    return [0 if pred_gw[i] == 0 and pred_mw[i] == -1 else -1 for i in range(len(pred_gw))]


def predict_samples_v2(goodware_clusterer, malware_clusterer, samples):
    # malware:1, goodware: 0 unknown:-1
    labels_gw, strengths_gw = hdbscan.approximate_predict(goodware_clusterer, samples)
    labels_mw, strengths_mw = hdbscan.approximate_predict(malware_clusterer, samples)
    pred_gw = [0 if l != -1 else -1 for l in labels_gw]
    pred_mw = [1 if l != -1 else -1 for l in labels_mw]

    predicts = []
    for i in range(len(pred_gw)):
        if pred_mw[i] == 1 and pred_gw[i] == -1:
            predicts.append(1)
        else:
            predicts.append(pred_gw[i])
    return predicts


def counter_predicts(predicts, print_=True):
    n_malwares = len([p for p in predicts if p == 1])
    n_goodwares = len([p for p in predicts if p == 0])
    n_unknowns = len(predicts) - n_malwares - n_goodwares
    if print_:
        print('malwares:', n_malwares)
        print('goodwares:', n_goodwares)
        print('unknowns:', n_unknowns)
    return n_malwares, n_goodwares, n_unknowns


class Cluster:

    def __init__(self):
        self.goodware_cluster = ai_utils.load_model(GOODWARE_CLUSTER_NAME)
        self.malware_cluster = ai_utils.load_model(MALWARE_CLUSTER_NAME)
        self.scaler = ai_utils.load_scaler(SCALER_NAME)

    def save(self):
        ai_utils.save_model(self.goodware_cluster, GOODWARE_CLUSTER_NAME)
        ai_utils.save_model(self.malware_cluster, MALWARE_CLUSTER_NAME)

    def train_(self, dataset_name=DATASET_NAME, save_data=True):
        dataset = pd.read_csv(dataset_name)

        X = dataset.iloc[:, :414].values
        y = dataset.iloc[:, 414].values

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
        self.train_with_data(X_train, X_test, y_train, y_test, save_data)

    def train_with_data(self, X_train, X_test, y_train, y_test, save_data=True):
        X_gw_train = [X_train[i] for i in range(len(y_train)) if y_train[i] == 0]
        X_mw_train = [X_train[i] for i in range(len(y_train)) if y_train[i] == 1]

        X_gw_test = [X_test[i] for i in range(len(y_test)) if y_test[i] == 0]
        X_mw_test = [X_test[i] for i in range(len(y_test)) if y_test[i] == 1]

        # Feature Scaling
        sc = ai_utils.load_scaler(SCALER_NAME)
        X_gw_train = sc.transform(X_gw_train)
        X_mw_train = sc.transform(X_mw_train)

        X_gw_test = sc.transform(X_gw_test)
        X_mw_test = sc.transform(X_mw_test)

        print('Goodware Cluster')
        gw_clusterer = ai_utils.generate_clusters(X_gw_train, min_cluster_size=10, min_samples=1, metric='euclidean',
                                                  plot_outliers=False)
        print('Malware Cluster')
        mw_clusterer = ai_utils.generate_clusters(X_mw_train, min_cluster_size=10, min_samples=1, metric='euclidean',
                                                  plot_outliers=False)

        # Evaluate
        (acc_gw, acc_mw, acc_total) = ai_utils.evaluate_cluster(X_gw_test, X_mw_test, gw_clusterer, 'goodware')
        print('(Goodware) -> acc_goodware:', acc_gw, ' | acc_malware:', acc_mw, ' | acc_total:', acc_total)
        (acc_gw, acc_mw, acc_total) = ai_utils.evaluate_cluster(X_gw_test, X_mw_test, mw_clusterer, 'malware')
        print('(Malware) -> acc_goodware:', acc_gw, ' | acc_malware:', acc_mw, ' | acc_total:', acc_total)

        self.goodware_cluster = gw_clusterer
        self.malware_cluster = mw_clusterer

        if save_data:
            self.save()

    def is_goodware(self, sample):
        predict = self.analyze([sample])
        return predict[0] == 0

    def analyze(self, samples):
        return predict_samples_v2(self.goodware_cluster, self.malware_cluster, self.scaler.transform(samples))
