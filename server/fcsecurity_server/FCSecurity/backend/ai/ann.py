# -*- coding: utf-8 -*-

# Importing the libraries
from keras.utils.vis_utils import plot_model
# import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from backend.ai.ai_utils import build_classifier, save_classifier, load_classifier, save_scaler, load_scaler#, plot_confusion_matrix

DATASET_NAME = 'backend/ai/dataset.csv'

MODEL_NAME = 'backend/ai/best_model.yaml'
WEIGHTS_NAME = 'backend/ai/best_model.h5'

SCALER_NAME = 'backend/ai/scaler.pkl'

VISUALIZATION_NAME = 'backend/ai/model.png'


class ANN:

    def __init__(self):
        self.classifier = load_classifier(model_name=MODEL_NAME, weights_name=WEIGHTS_NAME)
        self.scaler = load_scaler(SCALER_NAME)

    def save(self):
        save_scaler(self.scaler, name=SCALER_NAME)
        save_classifier(self.classifier, model_name=MODEL_NAME, weights_name=WEIGHTS_NAME)

    def save_visualization(self):
        ## Model Visualization
        plot_model(self.classifier, to_file=VISUALIZATION_NAME)

    def train(self, dataset_name=DATASET_NAME, use_save_scaler=True, save_data=True):
        dataset = pd.read_csv(dataset_name)

        X = dataset.iloc[:, :414].values
        y = dataset.iloc[:, 414].values
        # Splitting the dataset into the Training set and Test set

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
        self.train_with_data(X_train, X_test, y_train, y_test, use_save_scaler, save_data)

    def train_with_data(self, X_train, X_test, y_train, y_test, use_save_scaler=True, save_data=True):

        # Feature Scaling
        if use_save_scaler:
            sc = load_scaler(SCALER_NAME)
            X_train = sc.transform(X_train)
        else:
            sc = StandardScaler()
            X_train = sc.fit_transform(X_train)

        X_test = sc.transform(X_test)

        model = build_classifier(optimizer='adam', learn_rate=0.001, init='uniform', rate=0.3)

        hist = model.fit(X_train, y_train, batch_size=20, epochs=100)

        #
        # Evaluate
        score = model.evaluate(X_train, y_train, verbose=0)
        print("%s: %.2f%%" % (model.metrics_names[1], score[1] * 100))
        y_pred = model.predict(X_test)
        '''
        plot_confusion_matrix(y_pred, y_test, [0.3, 0.4, 0.5])

        # summarize history for accuracy
        plt.plot(hist.history['acc'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train'], loc='upper left')
        plt.show()

        # summarize history for loss
        plt.plot(hist.history['loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train'], loc='upper left')
        plt.show()
        '''
        self.classifier = model
        self.scaler = sc
        if save_data:
            self.save()

    def is_malware(self, sample, threshold=0.5):
        if self.classifier is None:
            return None
        g_row = sample[:414]
        new_prediction = self.classifier.predict(self.scaler.transform(np.array([g_row])))
        new_prediction = (new_prediction > threshold)
        return new_prediction[0][0]

    def analyze(self, samples, threshold=None):
        if self.classifier is None:
            return None
        predicts = [predict for predict in self.classifier.predict(self.scaler.transform(samples))]
        if threshold != None:
            predicts = (np.array(predicts) > threshold)
        return predicts

    def predict(self, samples, threshold):
        analyze = self.analyze(samples, threshold)
        return (np.array(analyze) > threshold)
