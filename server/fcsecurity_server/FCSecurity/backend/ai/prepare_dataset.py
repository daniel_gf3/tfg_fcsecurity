#!/usr/bin/python
import csv
from sys import argv
import optparse
import os
from os import listdir
from os.path import isfile, join
from backend.ai.prepare_report import PrepareReport, load_alexa_top
import progressbar


class Dataset:

    def __init__(self, path=None, training=False):
        self.errors = []
        self.rows = []
        self.fields = []
        self.training = training
        self.alexaTop = load_alexa_top()

        if path is None:
            self.raw_reports = None
            pass
        try:
            self.raw_reports = [join(path, f) for f in listdir(path) if isfile(join(path, f))]  # raw reports
        except:
            self.raw_reports = None

    def create_dataset(self):
        if not self.raw_reports:
            return False  # throw Exception
        print('Creating Dataset:')
        # setup progressbar
        bar = progressbar.ProgressBar(maxval=len(self.raw_reports),
                                      widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
        bar.start()
        i = 0
        # log errors
        for raw_report in self.raw_reports:
            try:
                pR = PrepareReport(raw_report, self.training, True)
                pR.set_alexa_top(self.alexaTop)
                report = pR.get_report()
                if not self.fields:
                    self.fields = self._get_fields(report)
                row = []
                for mainKey in report.keys():
                    for key in report[mainKey].keys():
                        item = report[mainKey][key]
                        if isinstance(item, bool):
                            row.append(int(item))
                        else:
                            row.append(item)
                self.rows.append(row)
            except Exception as inst:
                self.errors.append(raw_report)
                print(inst)
                continue
            i += 1
            bar.update(i)
        bar.finish()
        print('Dataset created!')
        return True

    def get_dataset_row(self, raw_report):
        try:
            pR = PrepareReport(raw_report, self.training, True)
            pR.set_alexa_top(self.alexaTop)
            report = pR.get_report()
            if not self.fields:
                self.fields = self._get_fields(report)
            row = []
            for mainKey in report.keys():
                for key in report[mainKey].keys():
                    item = report[mainKey][key]
                    if isinstance(item, bool):
                        row.append(int(item))
                    else:
                        row.append(item)
            return row
        except Exception as inst:
            self.errors.append(raw_report)
        return None

    def _get_fields(self, report):
        fields = []
        for mainKey in report.keys():
            for key in report[mainKey].keys():
                fields.append(mainKey + '_' + key)
        return fields

    def save_log_errors(self, save_path):
        errorsS = ""
        for e in self.errors:
            errorsS += e + "\n"
        with open('./errors.log', 'w') as file:
            file.write(errorsS)

    def save_dataset(self, save_path):
        if not self.rawReports:
            return None
        with open(save_path, 'w') as csvfile:
            # creating a csv writer object
            csvwriter = csv.writer(csvfile)

            # writing the fields
            csvwriter.writerow(self.fields)

            # writing the data rows
            csvwriter.writerows(self.rows)

    def _create_dir_if_needed(self, save_path):
        if not os.path.exists(os.path.dirname(save_path)):
            os.makedirs(os.path.dirname(save_path))


if __name__ == "__main__":
    """Create Dataset from a directory."""
    usage = 'usage: %prog (array-report directory) [destination directory]'
    parser = optparse.OptionParser(
        usage=usage,
        description='Create a dataset from the array-report found in the folder that is passed as parameter.'
                    'Dataset. Example '
                    'python %prog ./raw-reports/' './dataset/dataset.csv')

    parser.add_option(
        '-t', '--training', action='store_true', dest='training',
        help='get label from VirusTotal in order to train')

    (options, args) = parser.parse_args()
    training = options.training

    if not args:
        parser.error('No directory provided')

    originDirectory = args[0]
    if len(args) > 1:
        destination = args[1]
    else:
        reportDir = os.path.dirname(os.path.abspath(__file__)) + "/dataset/"
        reportName = "dataset.csv"
        destination = reportDir + reportName

    dataset = Dataset(originDirectory, training)
    datasetF = dataset.create_dataset()

    if not datasetF:
        print('Empty dataset')
    else:
        dataset.save_dataset(destination)
        dataset.save_log_errors(originDirectory + 'errors.log')
