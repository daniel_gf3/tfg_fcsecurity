from django.http import HttpResponseRedirect, HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from backend.serializers import AppResultSerializer
from backend.models import AppResult
from backend.utils import handle_uploaded_file, check_if_file_exists
import backend.tasks as task
from backend.utils import send_push
import json


# Create your views here.

@api_view(['GET', 'POST'])
def app_result(request, sha256=""):
    """
    Retrieve.
    """
    if request.method == 'GET':
        if sha256 == "":
            return Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            app_result = AppResult.objects.get(sha256=sha256)
        except AppResult.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = AppResultSerializer(app_result)
        return Response(serializer.data)
    elif request.method == 'POST':
        json_request = json.loads(request.body)
        print(json_request)

        app_result_mp = json_request['app_result']
        fcm_token = json_request['fcm']
        notify = json_request['notify'] == "1"
        print('APP_RESULT:', app_result_mp)
        print('fcm_token:', fcm_token)
        app_result_mp['positive'] = app_result_mp['positive'] == 1
        print('APP_RESULT_v2:', app_result_mp)

        serializer = AppResultSerializer(data=app_result_mp)
        if serializer.is_valid():
            serializer.save()
            send_push(app_result_mp['sha256'], app_result_mp['positive'], fcm_token, notify)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def upload_app(request):
    # import pdb;pdb.set_trace()
    sha256 = str(request.POST['sha256'])
    fcm_token = str(request.POST['fcm_token'])
    notify = str(request.POST['notify'])

    if check_if_file_exists(sha256):
        return Response({'result': 'Successful',
                         'sha256': sha256}, status=409)
    try:
        path = handle_uploaded_file(request.FILES['file'], sha256)
        task.analyze_app.delay(sha256, path, fcm_token, notify)
        return Response({'result': 'Successful',
                         'sha256': sha256}, status=202)
    except Exception:
        return Response({'result': 'Failed',
                         'sha256': sha256}, status=500)
