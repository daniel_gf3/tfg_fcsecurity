from rest_framework import serializers
from backend.models import AppResult


class AppResultSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AppResult
        fields = ('sha256', 'package_name', 'scan_date', 'positive')
        # read_only_fields = ('sha256')
