from django.db import models
import datetime


# Create your models here.

class AppResult(models.Model):
    sha256 = models.CharField(max_length=64, unique=True, default='test')
    package_name = models.CharField(max_length=50, default='test')
    positive = models.BooleanField(default=False)
    scan_date = models.DateField(default=datetime.date.today)

# python manage.py makemigrations
# python manage.py migrate
