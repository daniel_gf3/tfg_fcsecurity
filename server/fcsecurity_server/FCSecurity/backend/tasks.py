# Create your tasks here

from celery import shared_task
import backend.utils as utils
import backend.ai.ai_system as ai
import json
import requests


@shared_task
def analyze_app(sha256, path, fcm, notify):
    raw_report = utils.get_raw_report(path)
    raw_path = 'raw_reports/' + sha256 + '.json'
    raw_report.save_report(raw_path)
    general_report = raw_report.get_general_report()
    row_dataset = utils.get_row_dataset(raw_path)

    positive = ai.is_malware(row_dataset[:414])

    headers = {'content-type': 'application/json'}
    app_result = {'sha256': general_report["sha256"],
                  'package_name': general_report["package_name"],
                  'positive': 1 if positive else 0}

    content = {
        'fcm': fcm,
        'notify': notify,
        'app_result': app_result
    }
    r = requests.post('http://127.0.0.1:8000/app-result/', data=json.dumps(content), headers=headers)
    return 'file: ' + general_report["sha256"] + ' is: ' + ('malware' if positive else 'goodware')
