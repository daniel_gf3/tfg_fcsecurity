import os

from backend.ai.data_extraction.data_extraction import DataExtraction
from backend.ai.prepare_dataset import Dataset


def handle_uploaded_file(file, filename):
    if not os.path.exists('upload/'):
        os.mkdir('upload/')

    path = 'upload/' + filename

    with open(path, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return path


def check_if_file_exists(filename):
    from pathlib import Path

    my_file = Path("upload/" + filename)
    return my_file.exists()


'''
def calculate_SHA256(file):
    import hashlib
    sha256_hash = hashlib.sha256()
    with open(file, "rb") as f:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
        return sha256_hash.hexdigest()
    return "nose"
'''


def get_raw_report(apk_path):
    return DataExtraction(apk_path)


def get_row_dataset(raw_report_path):
    return Dataset(training=False).get_dataset_row(raw_report_path)


def send_push(sha256, positive, fcm_token, notify):
    import requests
    import json

    key = "AAAATJvyE1Y:APA91bHq6OhhtLDwAvMvcf5SGw6DjDyZp-4XO1wdBjAo-66Zck7azy_eMS3fxF1e5ZiEUCQ93yvaZ_4IhaxRtj0RksFwcG4-LJ87Un2OILoUbRedYnLkae2S2rr0yVaZ51VLN2HfVa1Op-eIvG5j1uQGraDCBlC28Q"

    data_raw = {"notification": {'title': '',
                                 'body': {'notify': notify, 'app_result': {'sha256': sha256, 'positive': positive}}},
                "registration_ids": [fcm_token, ]}
    headers = {"Content-Type": "application/json", "Authorization": "key={}".format(key)}
    result = requests.post("https://fcm.googleapis.com/fcm/send",
                           headers=headers,
                           data=json.dumps(data_raw),
                           verify=False)
