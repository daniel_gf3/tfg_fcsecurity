# -*- coding: utf-8 -*-

# Importing the libraries
import numpy as np
import pandas as pd

# Importing the dataset
dataset = pd.read_csv("dataset/dataset.csv")
X = dataset.iloc[:, :414].values
y = dataset.iloc[:, 414].values

#X = dataset.iloc[:300, :414].values
#y = dataset.iloc[:300, 414].values

# Encoding data
# new_dataset = dataset
# activities_size
# activities_size = new_dataset.loc[:, 'activities_size']
# max_activities_size = max(activities_size)
# new_dataset.loc[:, 'activities_size'] = activities_size / max_activities_size

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
X_train = sc.fit_transform(X_train)  # TODO revisar que hace este metodo realmente
X_test = sc.transform(X_test)

from ai_utils import save_scaler, load_scaler

save_scaler(sc, name='ori_sc.pkl')
# sc = load_scaler('ori_sc.pkl')


# Fitting the ANN to the training set
# classifier.fit(X_train, y_train, batch_size = 20, nb_epoch = 100)

# Predicting the Test set result
# y_pred = classifier.predict(X_test)
# y_pred = (y_pred > 0.5)


# create new prediction
# new_prediction = classifier.predict(sc.transform(np.array([[......]])))
# new_prediction = (new_prediction > 0.5)
# fin new prediction


# Evaluating the ANN
# Importing the Keras libraries and packages
# Tunning the ANN
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam, RMSprop

## configure GPU
'''
import tensorflow as tf
config = tf.ConfigProto( device_count = {'GPU': 1 , 'CPU': 56} ) 
sess = tf.Session(config=config) 
keras.backend.set_session(sess)
'''
##



def build_classifier(optimizer='adam', learn_rate=1, init='uniform', rate=0.2):
    if optimizer == 'adam':
        optimizer = Adam(lr=learn_rate)
    else:
        optimizer = RMSprop(lr=learn_rate)
    
    # Create classifier
    classifier = Sequential()

    classifier.add(Dense(500, kernel_initializer='uniform', activation='relu', input_dim=414))
    classifier.add(Dropout(rate=rate))
    classifier.add(Dense(500, activation='relu', kernel_initializer=init))
    classifier.add(Dropout(rate=rate))
    classifier.add(Dense(300, activation='relu', kernel_initializer=init))
    classifier.add(Dropout(rate=rate))
    classifier.add(Dense(1, activation='sigmoid', kernel_initializer=init))

    # Compile classifier
    classifier.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy', ])
    return classifier


classifier = KerasClassifier(build_fn=build_classifier, verbose=1)

parameters = {'batch_size': [10, 50, 100],
              'epochs': [10, 25, 50],
              'optimizer': ["adam", "rmsprop"],
              'learn_rate': [0.001, 0.01],
              'init': ['normal'],
              'rate': [0.2, 0.3]}

grid_search = GridSearchCV(estimator=classifier,
                           param_grid=parameters,
                           scoring='accuracy',
                           cv=10)
grid_search = grid_search.fit(X_train, y_train)
best_parameters = grid_search.best_params_
best_accuracy = grid_search.best_score_

accuracies = cross_val_score(estimator=classifier, X=X_train, y=y_train, cv=10, n_jobs=-1)
mean = accuracies.mean()
variance = accuracies.std()

## 
means = grid_search.cv_results_['mean_test_score']
stds = grid_search.cv_results_['std_test_score']
params = grid_search.cv_results_['params']

for mean_, stdev_, param_ in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean_, stdev_, param_))
        
print("Best: %f using %s" % (grid_search.best_score_, grid_search.best_params_))


######

tune_classifier = build_classifier(best_parameters['optimizer'], best_parameters['learn_rate'], best_parameters['init'], best_parameters['rate'])
tune_classifier.fit(X_train, y_train, batch_size = best_parameters['batch_size'], epochs = best_parameters['epochs'])

# evaluate the model
scores = tune_classifier.evaluate(X_train, y_train, verbose=0)
print("%s: %.2f%%" % (tune_classifier.metrics_names[1], scores[1] * 100))    

# #############################################################################
# Making the Confusion Matrix
from ai_utils import plot_confusion_matrix

y_pred = tune_classifier.predict(X_test)
plot_confusion_matrix(y_pred, y_test, [0.3, 0.4, 0.5])


# #############################################################################


from ai_utils import save_classifier, load_classifier

save_classifier(classifier=tune_classifier, model_name='tune_model.yaml', weights_name='tune_model.h5')

tune_classifier = load_classifier(model_name='tune_model.yaml', weights_name='tune_model.h5')

# evaluate loaded model on test data
tune_classifier.compile(loss='binary_crossentropy', optimizer=best_parameters['optimizer'], metrics=['accuracy'])
score = tune_classifier.evaluate(X_train, y_train, verbose=0)
print("%s: %.2f%%" % (tune_classifier.metrics_names[1], score[1] * 100))


## Old classifier
old_classifier = load_classifier(model_name='model.yaml', weights_name='model.h5')
old_classifier.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
old_score = old_classifier.evaluate(X_train, y_train, verbose=0)
print("%s: %.2f%%" % (old_classifier.metrics_names[1], old_score[1] * 100))

# Making the Confusion Matrix
y_pred = old_classifier.predict(X_test)
plot_confusion_matrix(y_pred, y_test, [0.3, 0.4, 0.5])


    
##### Build the BEST

best_model = build_classifier(optimizer='adam', learn_rate=0.001, init='uniform', rate=0.3)
best_model.fit(X_train, y_train, batch_size=20, epochs=100)

# Evaluate the best
best_score = best_model.evaluate(X_train, y_train, verbose=0)
print("%s: %.2f%%" % (best_model.metrics_names[1], best_score[1] * 100))

save_classifier(classifier=best_model, model_name='best_model.yaml', weights_name='best_model.h5')



model = load_classifier(model_name='best_model.yaml', weights_name='best_model.h5')





## Model Visualization
from keras.utils.vis_utils import plot_model
plot_model(best_model, to_file='best_model.png')