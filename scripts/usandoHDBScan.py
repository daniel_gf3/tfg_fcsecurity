#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 16:59:00 2018

@author: daniel
"""

import hdbscan
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
%matplotlib inline

# Importing the dataset
dataset = pd.read_csv("dataset/dataset.csv")
X = dataset.iloc[:, :414].values # remove sha256 and if is malware
y = dataset.iloc[:, 414].values
        
X_goodware = [X[i] for i in range(len(y)) if y[i] == 0]
X_malware = [X[i] for i in range(len(y)) if y[i] == 1]

# Splitting the dataset into the Training set and Test set
#from sklearn.model_selection import train_test_split

from ai_utils import load_scaler, save_cluster

sc = load_scaler('ori_sc.pkl')
X_goodware = sc.transform(X_goodware)
X_malware = sc.transform(X_malware)

X_gw_train = X_goodware[200:]
X_mw_train = X_malware[200:]

X_gw_test = X_goodware[:200]
X_mw_test = X_malware[:200]
    
    
def generate_clusters(type_, min_cluster_size, min_samples, metric = "euclidean", plot_outliers=True):
    
    if type_ == 'goodware':
        X_train = X_gw_train
    else:
        X_train = X_mw_train
    

    clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size, 
                                min_samples=min_samples, metric=metric, 
                                gen_min_span_tree=True,
                                allow_single_cluster=True).fit(X_train)
    
    labels = clusterer.labels_
    
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_outliers = len([l for l in labels if l == -1])
    
    
    # Show outlier...
    if plot_outliers:
        outliers = clusterer.outlier_scores_
        sns.distplot(clusterer.outlier_scores_[np.isfinite(clusterer.outlier_scores_)], rug=True) 
    
    # Predict cluster
    clusterer.generate_prediction_data()
    type(clusterer)
    
    # Calculate acc
    test_labels_gw, strengths_gw = hdbscan.approximate_predict(clusterer, X_gw_test)
    total_gw = len(X_gw_test)
    
    if type_ == 'goodware':
        success_gw = len([l for l in test_labels_gw if l != -1])
    else:
        success_gw = len([l for l in test_labels_gw if l == -1])
    acc_gw =  (success_gw * 100) / total_gw
    
    
    test_labels_mw, strengths_mw = hdbscan.approximate_predict(clusterer, X_mw_test)
    total_mw = len(X_mw_test)
    if type_ == 'goodware':
        success_mw = len([l for l in test_labels_mw if l == -1])
    else:
        success_mw = len([l for l in test_labels_mw if l != -1])
    acc_mw = (success_mw * 100) / total_mw
    
    success_total = success_gw + success_mw
    acc_total = (success_total * 100) / (len(X_gw_test) + len(X_mw_test))
    
    # 
    print('(', type_, ',', min_cluster_size, ',', min_samples, ') -> n_clusters:', n_clusters_, ' | n_outliers:', n_outliers, ' | acc_goodware:', acc_gw, ' | acc_malware:', acc_mw, ' | acc_total:', acc_total)
    
    return clusterer
    
    


def predict_sample_v1(goodware_clusterer, malware_clusterer, samples):
    # malware:1, goodware:-1, unknown:0 
    labels_gw, strengths_gw = hdbscan.approximate_predict(goodware_clusterer, samples)
    labels_mw, strengths_mw = hdbscan.approximate_predict(malware_clusterer, samples)
    predicts_gw = [-1 if l != -1 else 1 for l in labels_gw]
    predicts_mw = [1 if l != -1 else -1 for l in labels_mw]
    return [predicts_gw[i] if predicts_gw[i] == predicts_mw[i] else 0 for i in range(len(predicts_gw))]


def predict_sample_v2(goodware_clusterer, malware_clusterer, samples):
    # malware:1, goodware:-1, unknown:0 
    labels_gw, strengths_gw = hdbscan.approximate_predict(goodware_clusterer, samples)
    labels_mw, strengths_mw = hdbscan.approximate_predict(malware_clusterer, samples)
    pred_gw = [-1 if l != -1 else 1 for l in labels_gw]
    pred_mw = [1 if l != -1 else -1 for l in labels_mw]

    return [pred_gw[i] if pred_gw[i] == -1 or pred_gw[i] == pred_mw[i] else 0 for i in range(len(pred_gw))]
    

def predict_sample_v3(goodware_clusterer, malware_clusterer, samples):
    # malware:1, goodware:-1, unknown:0 
    labels_gw, strengths_gw = hdbscan.approximate_predict(goodware_clusterer, samples)
    pred_gw = [-1 if l != -1 else 1 for l in labels_gw]

    return [-1 if pred_gw[i] == -1 else 0 for i in range(len(pred_gw))]


def counter_predicts(predicts, print_=True):
    n_malwares =  len([p for p in predicts if p == 1])
    n_goodwares = len([p for p in predicts if p == -1])
    n_unknowns = len(predicts) - n_malwares - n_goodwares
    if print_:
        print('malwares:', n_malwares)
        print('goodwares:', n_goodwares)
        print('unknowns:', n_unknowns)
    return (n_malwares, n_goodwares, n_unknowns)

def predict_sample(goodware_clusterer, malware_clusterer, samples):
    predicts = predict_sample_v1(goodware_clusterer, malware_clusterer, samples)
    return [p if p==-1 else 0 for p in predicts]

'''
parameters = [('goodware', 70, None), ('goodware', 2, None), ('goodware', 2, 5),
              ('goodware', 20, 5), ('goodware', 10, 1), ('malware', 3, 1), ('malware', 2, 1)]
'''


parameters = [('goodware', 3, 2), ('goodware', 3, 1), ('goodware', 2, 1), ('goodware', 15, 1), ('malware', 2, 1)]

parameters = [('malware', 3, 2)]



for type_, min_cluster_size, min_samples in parameters:
    generate_clusters(type_, min_cluster_size, min_samples, plot_outliers=False)



goodware_clusterer = generate_clusters(type_='goodware', min_cluster_size=3, min_samples=1, metric='euclidean', plot_outliers=False)

malware_clusterer = generate_clusters(type_='malware', min_cluster_size=10, min_samples=1, metric='euclidean', plot_outliers=False)
print('\n')
print('malware_samples:')
predicts_mw_v1 = predict_sample_v1(goodware_clusterer, malware_clusterer, X_mw_test)
predicts_mw_v2 = predict_sample_v2(goodware_clusterer, malware_clusterer, X_mw_test)
predicts_mw_v3 = predict_sample_v3(goodware_clusterer, malware_clusterer, X_mw_test)
predicts_mw = predict_sample(goodware_clusterer, malware_clusterer, X_mw_test)
print('v1')
c_mw_v1 = counter_predicts(predicts_mw_v1)
print('v2')
c_mw_v2 = counter_predicts(predicts_mw_v2)
print('v3')
c_mw_v3 = counter_predicts(predicts_mw_v3)
print('v0')
c_mw = counter_predicts(predicts_mw)



print('\n')
print('goodware_samples:')
predicts_gw_v1 = predict_sample_v1(goodware_clusterer, malware_clusterer, X_gw_test)
predicts_gw_v2 = predict_sample_v2(goodware_clusterer, malware_clusterer, X_gw_test)
predicts_gw_v3 = predict_sample_v3(goodware_clusterer, malware_clusterer, X_gw_test)
predicts_gw = predict_sample(goodware_clusterer, malware_clusterer, X_gw_test)
print('v1')
c_gw_v1 = counter_predicts(predicts_gw_v1)
print('v2')
c_gw_v2 = counter_predicts(predicts_gw_v2)
print('v3')
c_gw_v3 = counter_predicts(predicts_gw_v3)
print('v0')
c_gw = counter_predicts(predicts_gw)






'''
for type_, min_cluster_size, min_samples in parameters:
    generate_clusters(type_, min_cluster_size, min_samples, metric='euclidean')
''' 
    
    
    
    
    