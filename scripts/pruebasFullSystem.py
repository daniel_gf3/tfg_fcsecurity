#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 22 01:15:37 2018

@author: daniel
"""
import numpy as np
import pandas as pd
from ai_utils import plot_confusion_matrix
from sklearn.model_selection import train_test_split
from cluster import Cluster
from ann import ANN


def is_malware(sample, threshold=0.3):
    from ann import ANN
    from cluster import Cluster
    if ANN().is_malware(sample) > threshold:
        return True
    if Cluster().analyze([sample]) == 1:
        return True
    return False


def analyze_v1(samples):
    from cluster import Cluster
    from ann import ANN
    cluster = Cluster()
    ann = ANN()    
    clusters_predicts = [-1 if p != 0 else p for p in cluster.analyze(samples)]
    need_ann = [samples[i] for i in range(len(samples)) if clusters_predicts[i] == -1]
    ann_predicts = list(ann.analyze(need_ann))
    return [float(ann_predicts.pop(0)) if clusters_predicts[i] == -1 else float(0.0) for i in range(len(samples))]



def analyze_v2(samples, threshold):
    return analyze_v3(samples, 0.0, threshold)



def analyze_v3(samples, min_threshold=0.2, max_threshold=0.3):
    from cluster import Cluster
    from ann import ANN
    ann = ANN()    
    ann_pd = ann.analyze(samples)
    
    need_cluster = []
    for i in range(len(samples)):
        if ann_pd[i] > min_threshold and ann_pd[i] < max_threshold:
            need_cluster.append(samples[i])
    print('need_cluster:', len(need_cluster))

    cluster = Cluster()
    clusters_pd = cluster.analyze(need_cluster)
    desconocidos = [r for r in clusters_pd if r == -1]
    print('desconoce:', len(desconocidos))
    result = []
    for i in range(len(samples)):
        if ann_pd[i] > min_threshold and ann_pd[i] < max_threshold:
            c_pd = clusters_pd.pop(0)
            if c_pd != -1:
                result.append(c_pd)
            else:
                result.append(ann_pd[i])
        else:
            result.append(ann_pd[i])
        
    return result
    
    
    
    
#    second_predicts = [float(clusters_pd[i]) if clusters_pd[i] != -1 else ann_pd[i][0] for i in range(len(clusters_pd))]
#    return [second_predicts.pop(0) if ann_pd[i] < min_threshold or ann_pd[i][0] > max_threshold else ann_pd[i] for i in range(len(samples))]

# #############################################################################
dataset = pd.read_csv('dataset/dataset.csv')


X = dataset.iloc[:, :414].values
y = dataset.iloc[:, 414].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Train clusters
cluster = Cluster()
cluster.train_with_data(X_train, X_test, y_train, y_test, save_data=True)

# Train ANN
ann = ANN()
ann.train_with_data(X_train, X_test, y_train, y_test, save_data=True)



#y_pred = analyze_v1(X_test)
#y_pred = analyze_v2(X_test, 0.3)
y_pred = analyze_v3(X_test, min_threshold=0.2, max_threshold=0.3)
plot_confusion_matrix(np.array(y_pred), y_test, [0.3])

for threshold in [0.3, 0.4, 0.5]:
    print('\n\nTHRESHOLD:', threshold)
    
    print('TEST_DATASET:')
    print('\nANN:')
    from ann import ANN
    y_pred = ANN().analyze(X_test)
    plot_confusion_matrix(np.array(y_pred), y_test, [threshold])
    
    print('\nFULL SYSTEM  v2:')
    y_pred = analyze_v2(X_test, threshold)
    plot_confusion_matrix(np.array(y_pred), y_test, [threshold])
    
    
    # ALL DATASET
    print('ALL_DATASET:')
    print('\nANN:')
    from ann import ANN
    y_pred = ANN().analyze(X)
    plot_confusion_matrix(np.array(y_pred), y, [threshold])
    
    print('\nFULL SYSTEM:')
    y_pred = analyze_v2(X, threshold=threshold)
    plot_confusion_matrix(np.array(y_pred), y, [threshold])


def analyze(samples, threshold=0.3):
    predicts = np.array(analyze_v2(samples, threshold))
    return (predicts > threshold)







