#!/usr/bin/python
import logging
import multiprocessing
import optparse
import os
import sys
import threading
from os.path import isfile, join

import queue

from dataset import Dataset
from prepare_report import PrepareReport

'''
SAMPLES_DIR = os.path.dirname(os.path.abspath(__file__)) + "/samples/"
# REPORTS_DIR = SAMPLES_DIR + "reports/"
# PARSE_REPORTS_DIR = SAMPLES_DIR + "parser-reports/"
ARRAY_REPORTS_DIR = SAMPLES_DIR + "array-reports/"
DATASET_DIR = SAMPLES_DIR + "dataset/"
'''

MAX_THREADS = int(multiprocessing.cpu_count())

scan_with_vt = False

LOGGING_LEVEL = logging.INFO  # Modify if you just want to focus on errors
logging.basicConfig(level=LOGGING_LEVEL,
                    format='%(asctime)s %(threadName)-10s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=sys.stdout)


def main():
    """Create dataset from apks files or raw_files"""
    usage = 'usage: %prog [options] <origin_dir> <dest_dataset>'

    parser = optparse.OptionParser(
        usage=usage,
        description="Prepare dataset from raw files"
    )

    parser.add_option(
        '-t', '--training', action='store_true', dest='training',
        help='get labels from VirusTotal in order to train')

    parser.add_option(
        '-r', '--rawdata', action='store_true', dest='is_raw_data',
        help='are files to scan or raw report'
    )

    (options, args) = parser.parse_args()
    print('args', args)

    if len(args) > 0:
        SAMPLES_DIR = args[0]
    else:
        SAMPLES_DIR = os.path.dirname(os.path.abspath(__file__)) + "/samples/"

    # REPORTS_DIR = SAMPLES_DIR + "reports/"
    # PARSE_REPORTS_DIR = SAMPLES_DIR + "parser-reports/"
    ARRAY_REPORTS_DIR = SAMPLES_DIR + "array-reports/"
    DATASET_DIR = SAMPLES_DIR + "dataset/"

    def create_reports(training, is_raw_data):
        end_process = False
        work = queue.Queue()  # Queue files to create report

        def worker():
            while not end_process:
                try:
                    path = work.get(True)
                    result = create_report(path, training, is_raw_data)
                    logging.info(result)
                except queue.Empty:
                    continue
                work.task_done()

        only_apks = [f for f in os.listdir(SAMPLES_DIR) if isfile(join(SAMPLES_DIR, f)) and not f.startswith('.')]

        # Prepare Queue
        for file in only_apks:
            work.put(file)

        # Create threads
        threads = []
        for thread_index in range(min(len(only_apks), MAX_THREADS)):
            thread = threading.Thread(target=worker)
            thread.daemon = True
            thread.start()
            threads.append(thread)

        try:
            logging.info('wait to end process')
            work.join()
            end_process = True
            logging.info("End Process")
        except KeyboardInterrupt:
            end_process = True
            logging.info('Stopping the process, initiated features extractions must finish')

    def create_report(file, training, is_raw_data):
        # Create report
        logging.info("CREATING REPORT... -> %s", file)
        pReport = PrepareReport(SAMPLES_DIR + file, training=training, is_raw_data=is_raw_data)
        report = pReport.get_report()
        if report == None:
            # logging.info("ERROR WITH SAMPLE %s", file)
            return "ERROR WITH SAMPLE " + file
        logging.info("CREATED REPORT %s", file)
        pReport.save_array_report(ARRAY_REPORTS_DIR + file)
        return 'SUCCESSFUL ' + file

    def create_dataset():
        try:
            dataset = Dataset(ARRAY_REPORTS_DIR)
            data = dataset.create_dataset()
            if data is None:
                return 'PROBLEM WITH DATASET'
            dataset.save_dataset(DATASET_DIR + "dataset.csv")
        except:
            logging.info('PROBLEM WITH DATASET')

    # call to code
    training = options.training
    is_raw_data = options.is_raw_data

    logging.info("CREATING REPORTS")
    logging.info("----------------")
    create_reports(training, is_raw_data)
    logging.info("CREATING DATASET")
    logging.info("---------------")
    create_dataset()
    logging.info("CREATED DATASET!!")
    logging.info("FINISHED!")


if __name__ == "__main__":
    main()
